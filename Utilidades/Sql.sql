CREATE TABLE public.categoria (
                cat_cod serial NOT NULL,
                cat_nome VARCHAR,
                cat_descricao VARCHAR,
                CONSTRAINT cat_cod PRIMARY KEY (cat_cod)
);

CREATE TABLE public.produto (
                prod_cod serial NOT NULL,
                prod_nome VARCHAR,
                prod_desc VARCHAR,
		prod_valor NUMERIC(10,2),
		prod_foto bytea,
                cat_cod INTEGER NOT NULL,
                CONSTRAINT prod_cod PRIMARY KEY (prod_cod)
);

CREATE TABLE public.usuario (
                usr_login VARCHAR NOT NULL,
                usr_cpf VARCHAR,
                usr_rg VARCHAR,
                usr_telefone VARCHAR,
                usr_senha VARCHAR,
                usr_salario NUMERIC(10,2),
                usr_endereco VARCHAR,
                usr_nome VARCHAR,
                usr_email VARCHAR,
                CONSTRAINT usr_login PRIMARY KEY (usr_login)
);

CREATE TABLE public.cliente (
                cli_cpf VARCHAR NOT NULL,
                cli_nome VARCHAR,
                cli_rg VARCHAR,
                cli_endereco VARCHAR,
                cli_telefone VARCHAR,
				cli_senha VARCHAR,
                cli_email VARCHAR,
                CONSTRAINT cli_cpf PRIMARY KEY (cli_cpf)
);

CREATE TABLE public.venda (
                ven_cod serial NOT NULL,
                ven_data DATE,
                cli_cpf VARCHAR NOT NULL,
                CONSTRAINT ok PRIMARY KEY (ven_cod)
);

CREATE TABLE public.produtos_venda (
		prodv_cod serial not null,
                prod_cod INTEGER NOT NULL,
                ven_cod INTEGER NOT NULL,
				prod_quant Integer,
                CONSTRAINT a PRIMARY KEY (prodv_cod, prod_cod, ven_cod)
);

CREATE TABLE public.recebimento (
                rec_cod serial NOT NULL,
                ven_cod INTEGER NOT NULL,
                rec_vencimento DATE,
                rec_num VARCHAR,
                rec_valor NUMERIC(10,2),
                rec_situacao VARCHAR,
                rec_pagamento DATE,
                CONSTRAINT rec_cod PRIMARY KEY (rec_cod, ven_cod)
);

ALTER TABLE public.produto ADD CONSTRAINT categoria_produto_fk
FOREIGN KEY (cat_cod)
REFERENCES public.categoria (cat_cod)
ON DELETE cascade
ON UPDATE NO ACTION
NOT DEFERRABLE;

ALTER TABLE public.produtos_venda ADD CONSTRAINT produto_produtos_venda_fk
FOREIGN KEY (prod_cod)
REFERENCES public.produto (prod_cod)
ON DELETE NO ACTION
ON UPDATE NO ACTION
NOT DEFERRABLE;

ALTER TABLE public.venda ADD CONSTRAINT cliente_venda_fk
FOREIGN KEY (cli_cpf)
REFERENCES public.cliente (cli_cpf)
ON DELETE cascade
ON UPDATE NO ACTION
NOT DEFERRABLE;

ALTER TABLE public.recebimento ADD CONSTRAINT venda_recebimento_fk
FOREIGN KEY (ven_cod)
REFERENCES public.venda (ven_cod)
ON DELETE cascade
ON UPDATE NO ACTION
NOT DEFERRABLE;

ALTER TABLE public.produtos_venda ADD CONSTRAINT venda_produtos_venda_fk
FOREIGN KEY (ven_cod)
REFERENCES public.venda (ven_cod)
ON DELETE cascade
ON UPDATE NO ACTION
NOT DEFERRABLE;
