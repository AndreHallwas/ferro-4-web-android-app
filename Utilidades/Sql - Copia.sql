
CREATE SEQUENCE public.categoria_cat_cod_seq_1;

CREATE TABLE public.categoria (
                cat_cod INTEGER NOT NULL DEFAULT nextval('public.categoria_cat_cod_seq_1'),
                cat_nome VARCHAR,
                cat_descricao VARCHAR,
                CONSTRAINT cat_cod PRIMARY KEY (cat_cod)
);


ALTER SEQUENCE public.categoria_cat_cod_seq_1 OWNED BY public.categoria.cat_cod;

CREATE SEQUENCE public.produto_prod_cod_seq;

CREATE TABLE public.produto (
                prod_cod INTEGER NOT NULL DEFAULT nextval('public.produto_prod_cod_seq'),
                prod_nome VARCHAR,
                prod_desc VARCHAR,
                cat_cod INTEGER NOT NULL,
                CONSTRAINT prod_cod PRIMARY KEY (prod_cod)
);


ALTER SEQUENCE public.produto_prod_cod_seq OWNED BY public.produto.prod_cod;

CREATE TABLE public.usuario (
                usr_login VARCHAR NOT NULL,
                usr_cpf VARCHAR,
                usr_rg VARCHAR,
                usr_telefone VARCHAR,
                usr_senha VARCHAR,
                usr_salario NUMERIC(10,2),
                usr_endereco VARCHAR,
                usr_nome VARCHAR,
                usr_email VARCHAR,
                CONSTRAINT usr_login PRIMARY KEY (usr_login)
);


CREATE TABLE public.cliente (
                cli_cpf VARCHAR NOT NULL,
                cli_nome VARCHAR,
                cli_rg VARCHAR,
                cli_endereco VARCHAR,
                cli_telefone VARCHAR,
                cli_email VARCHAR,
                CONSTRAINT cli_cpf PRIMARY KEY (cli_cpf)
);


CREATE SEQUENCE public.compra_com_cod_seq_1;

CREATE TABLE public.compra (
                com_cod INTEGER NOT NULL DEFAULT nextval('public.compra_com_cod_seq_1'),
                com_data DATE,
                cli_cpf VARCHAR NOT NULL,
                CONSTRAINT ok PRIMARY KEY (com_cod)
);


ALTER SEQUENCE public.compra_com_cod_seq_1 OWNED BY public.compra.com_cod;

CREATE TABLE public.produtos_compra (
                prod_cod INTEGER NOT NULL,
                com_cod INTEGER NOT NULL,
                CONSTRAINT a PRIMARY KEY (prod_cod, com_cod)
);


CREATE SEQUENCE public.recebimento_rec_cod_seq;

CREATE TABLE public.recebimento (
                rec_cod INTEGER NOT NULL DEFAULT nextval('public.recebimento_rec_cod_seq'),
                com_cod INTEGER NOT NULL,
                rec_vencimento DATE,
                rec_num VARCHAR,
                rec_valor NUMERIC(10,2),
                rec_situacao VARCHAR,
                rec_pagamento DATE,
                CONSTRAINT rec_cod PRIMARY KEY (rec_cod, com_cod)
);


ALTER SEQUENCE public.recebimento_rec_cod_seq OWNED BY public.recebimento.rec_cod;

ALTER TABLE public.produto ADD CONSTRAINT categoria_produto_fk
FOREIGN KEY (cat_cod)
REFERENCES public.categoria (cat_cod)
ON DELETE cascade
ON UPDATE NO ACTION
NOT DEFERRABLE;

ALTER TABLE public.produtos_compra ADD CONSTRAINT produto_produtos_compra_fk
FOREIGN KEY (prod_cod)
REFERENCES public.produto (prod_cod)
ON DELETE NO ACTION
ON UPDATE NO ACTION
NOT DEFERRABLE;

ALTER TABLE public.compra ADD CONSTRAINT cliente_compra_fk
FOREIGN KEY (cli_cpf)
REFERENCES public.cliente (cli_cpf)
ON DELETE cascade
ON UPDATE NO ACTION
NOT DEFERRABLE;

ALTER TABLE public.recebimento ADD CONSTRAINT compra_recebimento_fk
FOREIGN KEY (com_cod)
REFERENCES public.compra (com_cod)
ON DELETE cascade
ON UPDATE NO ACTION
NOT DEFERRABLE;

ALTER TABLE public.produtos_compra ADD CONSTRAINT compra_produtos_compra_fk
FOREIGN KEY (com_cod)
REFERENCES public.compra (com_cod)
ON DELETE cascade
ON UPDATE NO ACTION
NOT DEFERRABLE;
