<%-- 
    Document   : TelaLogin
    Created on : 19/10/2017, 11:35:56
    Author     : Aluno
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <link href="CSS/CssGeral.css" rel="stylesheet" type="text/css"/>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Tela Login</title>
    </head>
    <body>
        <div class="container">
            <div class="row">
                <div class="col-md-3 col-md-offset-4">
                    <div class="account-box">
                        <form class="form-signin" action="ValidaLogin">

                            <h2 class="form-signin-heading">Painel de Login</h2>

                            <p><label for="inputEmail" class="sr-only">Usuario</label>
                            <input type="text" name="Usr" class="form-control" placeholder="Usuario" required="" autofocus="">
                            </p>

                            <p><label for="inputPassword" class="sr-only">Senha</label>
                            <input type="password" name="Senha" class="form-control" placeholder="Senha com Números e Letras" required="">
                            </p>

                            <div class="checkbox">
                                <label>
                                    <input type="checkbox" value="remember-me"> Relembre-se
                                </label>
                            </div>

                            <button class="btn btn-lg btn-primary btn-block" type="submit">Sign in</button>
                            <p><div class="or-box row-block">
                                <div class="row">
                                    <div class="col-md-12 row-block">
                                        <a href="http://www.jquery2dotnet.com" class="btn btn-primary btn-block">Create New Account</a>
                                    </div>
                                </div>
                            </div></p>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>
