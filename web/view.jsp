<%@page import="Controladora.CtrlInterface"%>
<%@page import="entidades.Login"%>
<%@page import="UI.painelCentralView"%>
?<!DOCTYPE html>
<html lang="pt-br">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Sistema Web</title>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
        <link href="CSS/styles.css" rel="stylesheet" type="text/css"/>
        <script src="js/scripts.js" type="text/javascript"></script>
        <!--Cr�ditos Parciais:Web Dev Academy por id�ia de layout crud-->
    </head>
    
    <body>
        
         <header id="painel-topo">
            <nav class="navbar navbar-inverse navbar-fixed-top">
                <div class="container-fluid">
                    <div class="navbar-header">
                        <a class="navbar-brand" href="#">Sistema Web</a>
                    </div>
                    <div class="navbar-collapse collapse">
                        <ul id="painel-topo-opcoes" class="nav navbar-nav navbar-right">
                            <%
                                out.print(CtrlInterface.getPnTopoO().getPainelTopoOpcoes());
                            %>
                        </ul>
                    </div>
                </div>
            </nav>
        </header>
        <%
            String acao = request.getParameter("acao");
            if (Login.isLogado() == true) {
        %>
        <aside class="menu-lateral" id="menu_lateral">
            <nav class="navbar navbar-inverse navbar-left">
                <div class="container-fluid">
                    <div id="navbar_left" class="navbar-collapse collapse" >
                        <%
                            out.print(CtrlInterface.getPnLateralO().getPainelLateralOpcoes());
                        %>
                    </div>
                </div>
            </nav>
        </aside>               
        <article class="centro container-fluid" style="margin-top: 50px">
            <%
                out.print(painelCentralView.getpainelCentralView());
            %>
            <div id="actions" class="row">
                <div class="col-md-12">
                    <a href="list.jsp" class="btn btn-primary">Fechar</a>
                    <!--<a href="edit.html" class="btn btn-default">Editar</a>-->
                    <!--<a href="#" class="btn btn-default" data-toggle="modal" data-target="#delete-modal">Excluir</a>-->
                </div>
            </div>
        </article>
        <%}%>
    </body>
</html>