<%@page import="entidades.Login"%>
<%@page import="Controladora.CtrlInterface"%>
<!DOCTYPE html>
<html lang="pt-br">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Sistema Web</title>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
        <link href="CSS/styles.css" rel="stylesheet" type="text/css"/>
        <!--Cr�ditos Parciais:Web Dev Academy por id�ia de layout crud-->
    </head>
    <body>
        
        <header id="painel-topo">
            <nav class="navbar navbar-inverse navbar-fixed-top">
                <div class="container-fluid">
                    <div class="navbar-header">
                        <a class="navbar-brand" href="#">Sistema Web</a>
                    </div>
                    <div class="navbar-collapse collapse">
                        <ul id="painel-topo-opcoes" class="nav navbar-nav navbar-right">
                            <%
                                out.print(CtrlInterface.getPnTopoO().getPainelTopoOpcoes());
                            %>
                        </ul>
                    </div>
                </div>
            </nav>
        </header>
        <%
            String acao = request.getParameter("acao");
            if (Login.isLogado() == true) {
        %>
        <aside class="menu-lateral" id="menu_lateral">
            <nav class="navbar navbar-inverse navbar-left">
                <div class="container-fluid">
                    <div id="navbar_left" class="navbar-collapse collapse" >
                        <%
                            out.print(CtrlInterface.getPnLateralO().getPainelLateralOpcoes());
                        %>
                    </div>
                </div>
            </nav>
                    
        </aside>
                    
        <article id="centro" class="centro container-fluid">

            <%out.print("<h3 class='page-header'>" + CtrlInterface.getPnCampos().getAcao() + " itens" + "</h3>");%>
            
            <form method="POST" enctype="multipart/form-data" action="<%out.print(CtrlInterface.getPnCentralL().getPainelCentralController());%>" id="tabela-centro">
                <%  
                    out.print(CtrlInterface.getPnCampos().getPainelCampos());
                    out.print("<input type='hidden' name='tipo' value='"+CtrlInterface.getPnCampos().getValue()+"'/>");
                    out.print("<input type='hidden' name='id' value='"+(CtrlInterface.getPnCampos().getAcao().equals("alterar")? 1 : 0)+"'/>");
                    out.print("<div class='row'>");
                    out.print("<div class='col-md-12'>");
                    
                    if (CtrlInterface.getPnCampos().getAcao().equals("alterar")) {
                        out.print("<button type='submit' name='acao' value='Gravar' class='btn btn-primary'>Atualizar</button>");
                        out.print("<a href='list.jsp' class='btn btn-default'>Cancelar</a>");
                    }else if(CtrlInterface.getPnCampos().getAcao().equals("adicionar")){
                        out.print("<button type='submit' name='acao' value='Gravar' class='btn btn-primary'>Adicionar</button>");
                        out.print("<a href='list.jsp' class='btn btn-default'>Cancelar</a>");
                    }
                    
                    out.print("</div>");
                    out.print("</div>");
                %>
            </form>
        </article>
        <%}%>
    </body>
</html>