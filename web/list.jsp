<%@page import="entidades.Login"%>
<%@page import="Controladora.CtrlInterface"%>
?<!DOCTYPE html>
<html lang="pt-br">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Sistema Web</title>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
        <link href="CSS/styles.css" rel="stylesheet" type="text/css"/>
        <script src="js/scripts.js" type="text/javascript"></script>
        <!--Cr�ditos Parciais:Web Dev Academy por id�ia de layout crud-->
    </head>
    <body>
        <header id="painel-topo">
            <nav class="navbar navbar-inverse navbar-fixed-top">
                <div class="container-fluid">
                    <div class="navbar-header">
                        <a class="navbar-brand" href="#">Sistema Web</a>
                    </div>
                    <div class="navbar-collapse collapse">
                        <ul id="painel-topo-opcoes" class="nav navbar-nav navbar-right">
                            <%
                                out.print(CtrlInterface.getPnTopoO().getPainelTopoOpcoes());
                            %>
                        </ul>
                    </div>
                </div>
            </nav>
        </header>
        <%
            String acao = request.getParameter("acao");
            if (Login.isLogado() == true) {
        %>
        <aside class="menu-lateral" id="menu_lateral">
            <nav class="navbar navbar-inverse navbar-left">
                <div class="container-fluid">
                    <div id="navbar_left" class="navbar-collapse collapse" >
                        <%
                            out.print(CtrlInterface.getPnLateralO().getPainelLateralOpcoes());
                        %>
                    </div>
                </div>
            </nav>
        </aside>
        <article id="main" class="centro container-fluid" style="margin-top: 50px">
            <section id="top" class="row">
                <div class="col-sm-3">
                    <h2>Itens</h2>
                </div>
                <div class="col-sm-6">

                    <div class="input-group h2">
                        <input name="Busca" class="form-control" id="filtrar" type="text" placeholder="Pesquisar Itens">
                        <span class="input-group-btn">
                            <button class="btn btn-primary" type="submit" onclick="execBusca('<%out.print(CtrlInterface.getPnCentralL().getPainelCentralController());%>','<%out.print(CtrlInterface.getPnCentralL().getValue());%>')">
                                <span class="glyphicon glyphicon-search"></span>
                            </button>
                        </span>
                    </div>

                </div>
                <div class="col-sm-3">
                    <a href="<%out.print("CtrlInterface?acao=painelCampos&tipo="+CtrlInterface.getPnCentralL().getValue()+"&param=adicionar");%>" class="btn btn-primary pull-right h2">Adicionar</a>
                </div>
            </section> <!--Busca-->
            <hr />
            <!--Tela de Confirmar Exclusao-->
            <section class="modal fade" id="delete-modal" tabindex="-1" role="dialog" aria-labelledby="modalLabel">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Fechar"><span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title" id="modalLabel">Excluir Item</h4>
                        </div>
                        <div class="modal-body">
                            Deseja realmente excluir este item?
                        </div>
                        <div class="modal-footer">
                            <button type="button" onclick="execExclusao('<%out.print(CtrlInterface.getPnCentralL().getPainelCentralController());%>','<%out.print(CtrlInterface.getPnCentralL().getValue());%>')" data-dismiss="modal" class="btn btn-primary">Sim</button>
                            <button type="button" class="btn btn-default" data-dismiss="modal">N&atilde;o</button>
                        </div>
                    </div>
                </div>
            </section>
            <section id="list" class="row">
                <div class="table-responsive col-md-12">
                    <table class="table table-striped" cellspacing="0" cellpadding="0" id="tabela">
                        <%
                            out.print(CtrlInterface.getPnCentralL().getPainelCentralList());
                        %>
                    </table>
                </div>
            </section>
            <!--Pagina��o-->
            <section id="bottom" class="row">
                <div class="col-md-12">
                    <%
                        out.print("<ul class='pagination'>");
                        out.print("<li " + "class='disabled'" + "><a>&lt; Anterior</a></li>");
                        out.print("<li class='disabled'><a>1</a></li>");
                        for (int i = 2; i < 10; i++) {
                            out.print("<li><a href='" + "#" + "'>" + i + "</a></li>");
                        }
                        out.print("<li class='next'><a href='" + "#" + "' rel='next'>Pr�ximo &gt;</a></li>");
                        out.print("</ul>");
                    %>
                </div>
            </section> <!-- /#bottom -->
        </article> <!-- /#main -->
        <%}%>
    </body>
</html>