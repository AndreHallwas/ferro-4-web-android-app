/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entidades;

import entidades.Banco.Indice;
import entidades.Banco.ctrCategoria;
import entidades.ControledeEntidade;
import java.util.ArrayList;

/**
 *
 * @author Aluno
 */
public final class Categoria extends ControledeEntidade {

    private String Codigo;
    private String Nome;
    private String Descricao;

    public Categoria(String Codigo, String Nome, String Descricao) {
        this.Codigo = Codigo;
        this.Nome = Nome;
        this.Descricao = Descricao;
        setctrEntidade();
    }

    public Categoria() {
        setctrEntidade();
    }

    public String getCodigo() {
        return Codigo;
    }

    public String getNome() {
        return Nome;
    }

    public String getDescricao() {
        return Descricao;
    }

    public void setCodigo(String Codigo) {
        this.Codigo = Codigo;
    }

    public void setCodigo(int Codigo) {
        this.Codigo = Codigo + "";
    }

    public void setNome(String Nome) {
        this.Nome = Nome;
    }

    public void setDescricao(String Descricao) {
        this.Descricao = Descricao;
    }

    @Override
    void setctrEntidade() {
        Banco = Banco == null ? new ctrCategoria(this) : Banco;
        setCtrEntidadeWeb();
    }

    @Override
    public ArrayList<Indice> getIndices() {
        if (Indices == null) {
            Indices = new ArrayList();
            Indices.add(new Indice(Codigo, "cat_cod", "int", '1', "Código", "Codigo"));
            Indices.add(new Indice(Nome, "cat_nome", "str", "Nome"));
            Indices.add(new Indice(Descricao, "cat_descricao", "str", "Descrição", "Descricao"));
        } else {
            Indices.get(0).setAtributo(Codigo);
            Indices.get(1).setAtributo(Nome);
            Indices.get(2).setAtributo(Descricao);
        }
        return Indices;
    }

    @Override
    public void setCtrEntidadeWeb() {
        Tipo = "categoria";
    }

}
