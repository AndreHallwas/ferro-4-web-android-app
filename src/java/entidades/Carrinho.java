/*
 */
package entidades;

import entidades.Banco.Indice;
import entidades.Entidade;
import java.util.ArrayList;

/**
 *
 * @author Raizen
 */
public class Carrinho{

    private ArrayList<Itens> produtos;

    public Carrinho() {
        produtos = new ArrayList();
    }

    public boolean add(Produto produto, int Quantidade) {
        int Pos = buscaCodigo(produto);
        if (Pos != -1) {
            produtos.get(Pos).setQuantidade(produtos.get(Pos).getQuantidade() + Quantidade);
            return true;
        } else {
            return produtos.add(new Itens(produto, Quantidade));
        }
    }

    public boolean altera(Produto produto, int Quantidade) {
        int Pos = busca(produto);
        if (Pos != -1) {
            produtos.get(Pos).setQuantidade(Quantidade);
            return true;
        }
        return false;
    }

    public boolean remove(Produto produto) {
        int Pos = buscaCodigo(produto);
        if (Pos != -1) {
            produtos.remove(Pos);
            return true;
        }
        return false;
    }

    public int busca(Produto produto) {
        int i;
        for (i = 0; i < produtos.size()
                && produtos.get(i).getProduto().getNome().equalsIgnoreCase(produto.getNome()); i++) {
        }
        return i < produtos.size() ? i : -1;
    }

    public int buscaCodigo(Produto produto) {
        int i;
        for (i = 0; i < produtos.size()
                && !produtos.get(i).getProduto().getCodigo().equalsIgnoreCase(produto.getCodigo()); i++) {
        }
        return i < produtos.size() ? i : -1;
    }

    public ArrayList<Itens> get() {
        return produtos;
    }

    public String toHiperLine(String... Params) {
        String conteudo = "";
        for (int i = 0; i < produtos.size(); i++) {
            conteudo += ((EntidadeWeb) produtos.get(i)).toHiperLine(i,Params);
        }
        return conteudo;
    }

    public class Itens extends EntidadeWeb {

        private Produto produto;
        private int Quantidade;
        private String Valor;

        public Itens() {
        }

        public Itens(Produto produto, int Quantidade) {
            this.produto = produto;
            this.Quantidade = Quantidade;
            Valor = produto.getValor();
        }

        public Produto getProduto() {
            return produto;
        }

        public int getQuantidade() {
            return Quantidade;
        }

        public double getValor() {
            return Double.parseDouble(Valor);
        }

        public void setProduto(Produto produto) {
            this.produto = produto;
        }

        public void setQuantidade(int Quantidade) {
            this.Quantidade = Quantidade;
        }

        public void setValor(String Valor) {
            this.Valor = Valor;
        }

        @Override
        public ArrayList<Indice> getIndices() {
            if (Indices == null) {
                Indices = new ArrayList();
                Indices.add(new Indice(produto.getCodigo(), "", "str", '1', "Código", "Codigo"));
                Indices.add(new Indice(produto.getNome(), "", "str", "Nome"));
                Indices.add(new Indice(Quantidade, "", "int", "Quantidade"));
                Indices.add(new Indice(Valor, "", "str", "Valor"));
            } else {
                Indices.get(0).setAtributo(produto.getCodigo());
                Indices.get(1).setAtributo(produto.getNome());
                Indices.get(2).setAtributo(Quantidade);
                Indices.get(3).setAtributo(Valor);
            }
            return Indices;
        }

        @Override
        public void setCtrEntidadeWeb() {
            Tipo = "Carrinho";
        }

    }

}
