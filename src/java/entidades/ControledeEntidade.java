/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entidades;

import entidades.Banco.Indice;
import entidades.Banco.ctrEntidade;
import java.util.ArrayList;

/**
 *
 * @author Raizen
 */
public abstract class ControledeEntidade extends EntidadeWeb implements Entidade{
    protected ctrEntidade Banco;
    protected ArrayList<Indice> Indices;
    
    abstract void setctrEntidade();
    public abstract ArrayList<Indice> getIndices();
    
    @Override
    public boolean add() {
        return Banco.add();
    }

    @Override
    public boolean altera() {
        return Banco.altera();
    }

    @Override
    public boolean remove() {
        return Banco.remove();
    }

    @Override
    public ArrayList<Entidade> get(String filtro) {
        return Banco.get(filtro);
    }
    
    
}
