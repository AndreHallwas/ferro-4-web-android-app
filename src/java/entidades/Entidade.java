/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entidades;

import java.util.ArrayList;

/**
 *
 * @author Raizen
 */
public interface Entidade{
    public boolean add();
    public boolean altera();
    public boolean remove();
    public ArrayList<Entidade> get(String filtro);
}
