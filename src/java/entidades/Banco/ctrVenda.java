/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entidades.Banco;

import entidades.ControledeEntidade;
import entidades.Entidade;
import entidades.ProdutosVenda;
import entidades.Recebimento;
import entidades.Venda;
import java.util.ArrayList;

/**
 *
 * @author Raizen
 */
public class ctrVenda extends ControledeBanco {

    public ctrVenda(ControledeEntidade e) {
        super(e);
        Tabela = "Venda";
    }

    @Override
    public ArrayList<Entidade> get(String Filtro) {
        ProdutosVenda produto = new ProdutosVenda();
        Recebimento recebimento = new Recebimento();
        ArrayList<Entidade> vendas = super.get(Filtro);
        for (Entidade venda : vendas) {
            ((Venda) venda).setProdutosdaVenda(produto.get(((Venda) venda).getCodigo()));
            ((Venda) venda).setRecebimentos(recebimento.get(((Venda) venda).getCodigo()));
        }
        return vendas;
    }

    @Override
    public boolean add() {
        boolean var = true;
        setControl(1);
        var = var && super.add();
        setControl(1);
        Venda venda = (Venda) e;
        String Codigo = Integer.toString(Utils.Banco.getCon().getMaxPK("venda", "ven_cod"));
        venda.setCodigo(Codigo);
        
        ArrayList<ProdutosVenda> produtos = ((Venda) e).getProdutosdaVenda();
        ArrayList<Recebimento> recebimentos = ((Venda) e).getRecebimentos();
        if (produtos != null && recebimentos != null) {
            for (ProdutosVenda produto : produtos) {
                produto.setCompra(venda);
                var = var && produto.add();
            }
            for (Recebimento recebimento : recebimentos) {
                recebimento.setCompra(venda);
                var = var && recebimento.add();
            }
            /////super.altera();
        }
        return var;
    }

}
