/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entidades.Banco;

/**
 *
 * @author Raizen
 */
public class Indice {
    private Object Atributo;
    private String Indice;
    private String Tipo;
    private String Nome;
    private String Campo;
    private char Chave;

    public Indice(Object Atributo, String Indice, String Tipo, String Nome) {
        this.Atributo = Atributo;
        this.Indice = Indice;
        this.Tipo = Tipo;
        this.Nome = Nome;
        this.Campo = Nome;
        Chave = '0';
    }
    
    public Indice(Object Atributo, String Indice, String Tipo, String Nome, Object Campo) {
        this.Atributo = Atributo;
        this.Indice = Indice;
        this.Tipo = Tipo;
        this.Nome = Nome;
        this.Campo = Campo.toString();
        Chave = '0';
    }

    public Indice(Object Atributo, String Indice, String Tipo, char Chave, String Nome) {
        this.Atributo = Atributo;
        this.Indice = Indice;
        this.Tipo = Tipo;
        this.Nome = Nome;
        this.Campo = Nome;
        this.Chave = Chave;
    }
    
    public Indice(Object Atributo, String Indice, String Tipo, char Chave, String Nome, Object Campo) {
        this.Atributo = Atributo;
        this.Indice = Indice;
        this.Tipo = Tipo;
        this.Nome = Nome;
        this.Chave = Chave;
        this.Campo = Campo.toString();
    }

    public void setChave(char Chave) {
        this.Chave = Chave;
    }

    public char getChave() {
        return Chave;
    }

    public Object getAtributo() {
        return Atributo;
    }

    public String getIndice() {
        return Indice;
    }

    public String getTipo() {
        return Tipo;
    }

    public void setAtributo(Object Atributo) {
        this.Atributo = Atributo;
    }

    public void setIndice(String Indice) {
        this.Indice = Indice;
    }

    public void setTipo(String Tipo) {
        this.Tipo = Tipo;
    }

    public String getNome() {
        return Nome;
    }

    public void setNome(String Nome) {
        this.Nome = Nome;
    }

    public String getCampo() {
        return Campo;
    }

    public void setCampo(String Campo) {
        this.Campo = Campo;
    }
    
    
}
