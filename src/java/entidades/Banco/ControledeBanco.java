/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entidades.Banco;

import Utils.Banco;
import Utils.Imagem;
import Utils.Mensagem;
import entidades.ControledeEntidade;
import entidades.Entidade;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.lang.reflect.Method;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.time.LocalDate;
import java.util.ArrayList;

/**
 *
 * @author Raizen
 */
public abstract class ControledeBanco implements ctrEntidade {

    protected boolean flag;
    protected PreparedStatement pstmt;
    protected String Msg;
    protected String Tabela;
    protected ArrayList<Indice> Indices;
    protected Class<?> Type;
    protected ControledeEntidade e;
    protected int Control = 1;

    public ControledeBanco(ControledeEntidade e) {
        try {
            setE(e);
            setType(e.getClass());
            setIndices(e.getIndices());
        } catch (Exception ex) {
        }
    }

    @Override
    public boolean add() {
        int Pos = 0;
        setIndices();
        if (Indices.size() > 0 && Control == 1) {
            String sqlCampos = "", sqlParametros = "";

            while (Pos < Indices.size() && (Indices.get(Pos).getAtributo() == null
                    || Indices.get(Pos).getAtributo().toString().isEmpty() || Indices.get(Pos).getChave() == '5')) {
                Pos++;
            }
            if (Pos < Indices.size()) {
                sqlCampos += "(" + Indices.get(Pos).getIndice();
                sqlParametros += "(?";
                for (int i = Pos + 1; i < Indices.size(); i++) {
                    if (Indices.get(i).getAtributo() != null && Indices.get(i).getChave() != '5') {
                        sqlCampos += "," + Indices.get(i).getIndice();
                        sqlParametros += ",?";
                    }
                }
                sqlCampos += ")";
                sqlParametros += ")";
                pstmt = Banco.getCon().geraStatement("insert into " + Tabela + " " + sqlCampos + " values" + sqlParametros);
                setParametros();
                setMsg(flag = Banco.getCon().manipular(pstmt), "Cadastro");
            }
        }
        return flag;
    }

    @Override
    public boolean altera() {
        setIndices();
        int Pos = 0;
        if (Indices.size() > 0 && Control == 1) {
            while (Pos < Indices.size() && (Indices.get(Pos).getAtributo() == null
                    || Indices.get(Pos).getAtributo().toString().isEmpty() || Indices.get(Pos).getChave() == '5')) {
                Pos++;
            }
            if (Pos < Indices.size()) {
                String sqlCampos = "", sqlParam = "";
                sqlCampos = Indices.get(Pos).getIndice() + " = ?";
                if (Indices.get(Pos).getChave() == '1') {
                    sqlParam = Indices.get(Pos).getIndice() + " = '" + Indices.get(Pos).getAtributo() + "'";
                }
                for (int i = Pos + 1; i < Indices.size(); i++) {
                    if (Indices.get(i).getAtributo() != null && Indices.get(i).getChave() != '5') {
                        sqlCampos += ", " + Indices.get(i).getIndice() + " = ?";
                        if (Indices.get(i).getChave() == '1') {
                            sqlParam = Indices.get(i).getIndice() + " = '" + Indices.get(i).getAtributo() + "'";
                        }
                    }
                }
                sqlCampos += " where " + sqlParam;
                pstmt = Banco.getCon().geraStatement("update " + Tabela + " set " + sqlCampos);
                setParametros();
                setMsg(flag = Banco.getCon().manipular(pstmt), "Alteração");
            }
        }
        return flag;
    }

    @Override
    public boolean remove() {
        setIndices();
        int i = 0;
        while (i < Indices.size() && !(Indices.get(i).getChave() == '1')) {
            i++;
        }
        if (i < Indices.size()) {
            pstmt = Banco.getCon().geraStatement("delete from " + Tabela + " where " + Indices.get(i).getIndice()
                    + " = '" + Indices.get(i).getAtributo() + "'");
            setMsg(flag = Banco.getCon().manipular(pstmt), "Exclusão");
        }
        return flag;
    }

    /*
    public ResultSet get(String filtro) {
        String sql = "";
        ResultSet rs = null;
        boolean First = false;
        try {
            sql = "select * from " + Tabela + " ";
            ArrayList<Indice> Indices = e.getIndices();
            if (!filtro.isEmpty()) {
                if (Indices.size() > 0) {
                    sql += "where ";
                    for (Indice indice : Indices) {
                        if ("int".equals(indice.getTipo()) || "double".equals(indice.getTipo())) {
                            sql += getCodigo(indice.getIndice(), filtro, First);
                            First = true;
                        } else if (First) {
                            sql += " or upper(" + indice.getIndice() + ") like upper('" + filtro + "%') ";
                        } else if (!First) {
                            sql += " upper(" + indice.getIndice() + ") like upper('" + filtro + "%') ";
                            First = true;
                        }
                    }
                }
            }
            pstmt = Banco.getCon().geraStatement(sql);
            rs = Banco.getCon().consultar(pstmt);

            setMsg(true, "Consulta");

        } catch (Exception ex) {
            Msg = "Erro: " + ex.getMessage();
            Mensagem.ExibirException(ex);
        }
        return rs;
    }
     */
    @Override
    public ArrayList<Entidade> get(String filtro) {
        setIndices();
        String sql = "";
        ResultSet rs = null;
        boolean First = false;
        ControledeEntidade ce = null;
        ArrayList<Entidade> Entidades = new ArrayList();
        try {
            sql = "select * from " + Tabela + " ";
            if (!filtro.isEmpty()) {
                if (Indices.size() > 0) {
                    sql += "where ";
                    for (Indice indice : Indices) {
                        if (indice.getChave() != '5') {
                            if ("int".equals(indice.getTipo()) || "double".equals(indice.getTipo())) {
                                sql += getCodigo(indice.getIndice(), filtro, First);
                                First = !sql.isEmpty();
                            } else if (First) {
                                if ("date".equals(indice.getTipo())) {
                                    if (filtro.length() > 8) {
                                        sql += " or " + indice.getIndice() + " = '" + filtro + "'";
                                    }
                                } else if ("img".equals(indice.getTipo())) {
                                    sql += " or " + indice.getIndice() + " = '" + filtro + "'";
                                } else {
                                    sql += " or upper(" + indice.getIndice() + ") like upper('" + filtro + "%') ";
                                }
                            } else if (!First) {
                                if ("date".equals(indice.getTipo())) {
                                    if (filtro.length() > 8) {
                                        sql += " " + indice.getIndice() + " = '" + filtro + "'";
                                        First = true;
                                    }
                                } else if ("img".equals(indice.getTipo())) {
                                    sql += " " + indice.getIndice() + " = '" + filtro + "'";
                                    First = true;
                                } else {
                                    sql += " upper(" + indice.getIndice() + ") like upper('" + filtro + "%') ";
                                    First = true;
                                }
                            }
                        }
                    }
                }
            }

            pstmt = Banco.getCon().geraStatement(sql);
            rs = Banco.getCon().consultar(pstmt);
            while (rs.next()) {
                ce = (ControledeEntidade) getObject(Type.getName(), Type.getClass(), " ", 1, "");
                for (int i = 0; i < Indices.size(); i++) {
                    if (Indices.get(i).getChave() != '5') {
                        getObject("", ce, "set" + Indices.get(i).getCampo(), 0, getParameterType(rs, i));
                    }
                }
                Entidades.add(ce);
            }
            setMsg(true, "Consulta");

        } catch (Exception ex) {
            Msg = "Erro: " + ex.getMessage();
            Mensagem.ExibirException(ex);
        }
        return Entidades;
    }

    public void setIndices() {
        Indices = e.getIndices();
    }

    public boolean setParametros() {
        flag = true;
        try {
            for (int i = 1, j = 0; j < Indices.size(); i++, j++) {
                if (!(Indices.get(j).getAtributo() == null
                        || Indices.get(j).getAtributo().toString().isEmpty() || Indices.get(j).getChave() == '5')) {
                    switch (Indices.get(j).getTipo()) {
                        case "int":
                            pstmt.setInt(i, Integer.parseInt(Indices.get(j).getAtributo() + ""));
                            break;
                        case "str":
                            pstmt.setString(i, (String) Indices.get(j).getAtributo());
                            break;
                        case "date":
                            pstmt.setDate(i, (Date.valueOf((LocalDate) Indices.get(j).getAtributo())));/*LocalDate*/
                            break;
                        case "double":
                            pstmt.setDouble(i, Double.parseDouble(Indices.get(j).getAtributo() + ""));
                            break;
                        case "img":
                            byte[] a = {1};
                            pstmt.setBinaryStream(i, (Indices.get(j).getAtributo() != null)
                                    ? Imagem.BufferedImageToInputStream((BufferedImage) Indices.get(j).getAtributo())
                                    : new ByteArrayInputStream((a)));
                            break;
                        default:
                            pstmt.setObject(i, Indices.get(j).getAtributo());
                            break;
                    }
                } else {
                    i--;
                }
            }
        } catch (Exception ex) {
            Mensagem.ExibirException(ex);
            flag = false;
        }
        return flag;
    }

    protected Object getParameterType(ResultSet rs, int j) {
        try {
            switch (Indices.get(j).getTipo()) {
                case "int":
                    return rs.getInt((String) Indices.get(j).getIndice());
                case "str":
                    return rs.getString((String) Indices.get(j).getIndice());
                case "date":
                    return rs.getDate((String) Indices.get(j).getIndice());/*LocalDate*/
                case "double":
                    return rs.getDouble((String) Indices.get(j).getIndice());
                case "img":
                    return Imagem.ByteArrayToBufferedImage(rs.getBytes((String) Indices.get(j).getIndice()));
                default:
                    return rs.getObject((String) Indices.get(j).getIndice());
            }
        } catch (Exception ex) {
            Mensagem.ExibirException(ex);
        }
        return null;
    }

    public Object getObject(String nomeClasse, Object obj, String Metodo, int flag, Object... args) {

        try {
            Class cls = Class.forName(nomeClasse);
            obj = cls.newInstance();
            if (flag == 1) {
                return obj;
            }
        } catch (Exception e) {
            Mensagem.ExibirException(e);
        }

        Class[] params = {};
        if (args.length > 0) {
            params = new Class[args.length];
            for (int i = 0; i < args.length; i++) {
                if (args[i] instanceof Integer) {
                    params[i] = Integer.TYPE;
                } else if (args[i] instanceof Character) {
                    params[i] = Character.TYPE;
                } else if (args[i] instanceof Double) {
                    params[i] = Double.TYPE;
                } else if (args[i] instanceof Float) {
                    params[i] = Float.TYPE;
                } else if (args[i] instanceof Long) {
                    params[i] = Long.TYPE;
                } else if (args[i] == null) {
                    return true;
                } else if (args[i] instanceof java.sql.Date) {
                    params[i] = LocalDate.class;
                } else {
                    params[i] = args[i].getClass();/*Integer.TYPE*/
                }
            }
        }
        try {
            /////obj.getClass().getMethods()
            Method method = obj.getClass().getMethod(Metodo, params);
            method.invoke(obj, args);

        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return true;
    }

    protected void setMsg(boolean Condition, String Op) {
        if (Condition) {
            Msg = Op + " Efetuado";
            flag = true;
        } else {
            Msg = Op + " Não Efetuado Erro: " + Banco.getCon().getMensagemErro();
        }
        Mensagem.ExibirLog(Msg);
    }

    protected String getCodigo(String Campo, String filtro, boolean op) {
        String sql = "";
        if (!filtro.isEmpty()) {
            try {
                int aux = Integer.parseInt(0 + filtro);
                if (op) {
                    sql = sql + " or " + Campo + " = '" + aux + "'";
                } else {
                    sql = sql + " " + Campo + " = '" + aux + "'";
                }
                return sql;
            } catch (NumberFormatException ex) {
                Mensagem.ExibirException(ex, "Erro no Nivel Cod");
            }
        }
        return "";
    }

    public static boolean isNumeric(String str) {
        if (str == null) {
            return false;
        }
        int sz = str.length();
        for (int i = 0; i < sz; i++) {
            if (Character.isDigit(str.charAt(i)) == false) {
                return false;
            }
        }
        return true;
    }

    public boolean isFlag() {
        return flag;
    }

    public PreparedStatement getPstmt() {
        return pstmt;
    }

    public String getMsg() {
        return Msg;
    }

    public int getControl() {
        return Control;
    }

    public void setControl(int Control) {
        this.Control = Control;
    }

    public ArrayList<Indice> getIndices() {
        return Indices;
    }

    public Class<?> getType() {
        return Type;
    }

    public void setIndices(ArrayList<Indice> Indices) {
        this.Indices = Indices;
    }

    public void setType(Class<?> Type) {
        this.Type = Type;
    }

    public ControledeEntidade getE() {
        return e;
    }

    public void setE(ControledeEntidade e) {
        this.e = e;
    }

}
