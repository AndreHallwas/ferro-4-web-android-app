/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entidades;

import entidades.Pessoa;

/**
 *
 * @author Raizen
 */
public class Login {
    private static Pessoa individuo;
    private static boolean logado = false;

    private Login() {
    }
    
    public static void logar(Pessoa individuo){
        Login.individuo = individuo;
        Login.logado = true;
    }
    
    public static void deslogar(){
        Login.logado = false;
    }

    public static boolean isLogado() {
        return Login.logado;
    }

    public static Pessoa getIndividuo() {
        return individuo;
    }
    
    
    
    
    
    
}
