/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entidades;

import entidades.Banco.Indice;
import entidades.Banco.ctrCategoria;
import entidades.Banco.ctrPessoa;
import java.util.ArrayList;

/**
 *
 * @author Aluno
 */
public abstract class Pessoa extends ControledeEntidade{
    protected String Nome;
    protected String RG;
    protected String CPF;
    protected String Endereco;
    protected String Telefone;
    protected String Email;
    private int tipo;

    public Pessoa(int tipo) {
        this.tipo = tipo;
    }

    public Pessoa(String Nome, String RG, String CPF, String Endereco, String Telefone, String Email,int tipo) {
        this.Nome = Nome;
        this.RG = RG;
        this.CPF = CPF;
        this.Endereco = Endereco;
        this.Telefone = Telefone;
        this.Email = Email;
        this.tipo = tipo;
    }
    
    public String getNome() {
        return Nome;
    }

    public String getRG() {
        return RG;
    }

    public String getCPF() {
        return CPF;
    }

    public String getEndereco() {
        return Endereco;
    }

    public String getTelefone() {
        return Telefone;
    }

    public String getEmail() {
        return Email;
    }

    public void setNome(String Nome) {
        this.Nome = Nome;
    }

    public void setRG(String RG) {
        this.RG = RG;
    }

    public void setCPF(String CPF) {
        this.CPF = CPF;
    }

    public void setEndereco(String Endereco) {
        this.Endereco = Endereco;
    }

    public void setTelefone(String Telefone) {
        this.Telefone = Telefone;
    }

    public void setEmail(String Email) {
        this.Email = Email;
    }
    
    @Override
    public ArrayList<Indice> getIndices() {
        if (Indices == null) {
            Indices = new ArrayList();
            Indices.add(new Indice(Nome, "cli_nome", "str", "Nome"));
            Indices.add(new Indice(RG, "cli_rg", "str", "RG"));
            Indices.add(new Indice(CPF, "cli_cpf", "str",'1', "CPF"));
            Indices.add(new Indice(Endereco, "cli_endereco", "str", "Endereço", "Endereco"));
            Indices.add(new Indice(Telefone, "cli_telefone", "str", "Telefone"));
            Indices.add(new Indice(Email, "cli_email", "str", "E-mail", "Email"));
        }else{
            Indices.get(0).setAtributo(Nome);
            Indices.get(1).setAtributo(RG);
            Indices.get(2).setAtributo(CPF);
            Indices.get(3).setAtributo(Endereco);
            Indices.get(4).setAtributo(Telefone);
            Indices.get(5).setAtributo(Email);
        }
        return Indices;
    }
    
    @Override
    void setctrEntidade() {
        Banco = Banco == null ? new ctrPessoa(tipo,this) : Banco;
    }
    
}
