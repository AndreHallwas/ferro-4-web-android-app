/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entidades;

import Controladora.CtrlVenda;
import entidades.Banco.Indice;
import entidades.Banco.ctrCategoria;
import entidades.Banco.ctrEntidade;
import entidades.Banco.ctrRecebimento;
import entidades.ControledeEntidade;
import java.time.LocalDate;
import java.util.ArrayList;

/**
 *
 * @author Aluno
 */
public class Recebimento extends ControledeEntidade{
    public String Codigo;
    public Venda compra;
    public LocalDate Vencimento;
    public LocalDate Pagamento;
    public String NumerodaParcela;
    public String Valor;
    public String Situacao;

    public Recebimento(String Codigo, Venda compra, LocalDate Vencimento, LocalDate Pagamento, String NumerodaParcela, String Valor, String Situacao) {
        this.Codigo = Codigo;
        this.compra = compra;
        this.Vencimento = Vencimento;
        this.Pagamento = Pagamento;
        this.NumerodaParcela = NumerodaParcela;
        this.Valor = Valor;
        this.Situacao = Situacao;
        setctrEntidade();
    }

    public Recebimento(Venda compra, LocalDate Vencimento, String NumerodaParcela, String Valor) {
        this.compra = compra;
        this.Vencimento = Vencimento;
        this.NumerodaParcela = NumerodaParcela;
        this.Valor = Valor;
        Situacao = "Não Pago";
        setctrEntidade();
    }

    public Recebimento() {
        setctrEntidade();
    }

    public String getCodigo() {
        return Codigo;
    }

    public Venda getCompra() {
        return compra;
    }

    public LocalDate getVencimento() {
        return Vencimento;
    }

    public LocalDate getPagamento() {
        return Pagamento;
    }

    public String getNumerodaParcela() {
        return NumerodaParcela;
    }

    public String getValor() {
        return Valor;
    }

    public String getSituacao() {
        return Situacao;
    }

    public void setCodigo(String Codigo) {
        this.Codigo = Codigo;
    }

    public void setCompra(Venda compra) {
        this.compra = compra;
    }

    public void setVencimento(LocalDate Vencimento) {
        this.Vencimento = Vencimento;
    }

    public void setPagamento(LocalDate Pagamento) {
        this.Pagamento = Pagamento;
    }

    public void setNumerodaParcela(String NumerodaParcela) {
        this.NumerodaParcela = NumerodaParcela;
    }

    public void setValor(String Valor) {
        this.Valor = Valor;
    }
    
    public void setValor(double Valor) {
        this.Valor = Valor+"";
    }

    public void setSituacao(String Situacao) {
        this.Situacao = Situacao;
    }

    @Override
    void setctrEntidade() {
        Banco = Banco == null ? new ctrRecebimento(this) : Banco;
    }
    
    public void setCodigo(int Codigo) {
        this.Codigo = Codigo+"";
        setCtrEntidadeWeb();
    }
    
    public void setCompra(int Venda){
        /////this.compra = (Venda) CtrlVenda.getEntidadeE(Venda+"", new Venda());
    }

    @Override
    public ArrayList<Indice> getIndices() {
        if(Indices == null){
            Indices = new ArrayList();
            Indices.add(new Indice(Vencimento, "rec_vencimento","date","Vencimento"));
            Indices.add(new Indice(Pagamento, "rec_pagamento","date","Pagamento"));
            Indices.add(new Indice(NumerodaParcela, "rec_num","str","NumerodaParcela","NumerodaParcela"));
            Indices.add(new Indice(Valor, "rec_valor","double","Valor"));
            Indices.add(new Indice(Situacao, "rec_situacao","str","Situação","Situacao"));
            Indices.add(new Indice(Codigo, "rec_cod","int",'1',"Código","Codigo"));
            Indices.add(new Indice(compra != null ? compra.getCodigo() : null, "ven_cod","int",'2',"Compra"));
        }else{
            Indices.get(0).setAtributo(Vencimento);
            Indices.get(1).setAtributo(Pagamento);
            Indices.get(2).setAtributo(NumerodaParcela);
            Indices.get(3).setAtributo(Valor);
            Indices.get(4).setAtributo(Situacao);
            Indices.get(5).setAtributo(Codigo);
            Indices.get(6).setAtributo(compra != null ? compra.getCodigo() : null);
        }
        return Indices;
    }

    @Override
    public void setCtrEntidadeWeb() {
        Tipo = "recebimento";
    }
}
