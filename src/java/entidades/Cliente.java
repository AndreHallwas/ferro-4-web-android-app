/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entidades;

import entidades.Banco.Indice;
import entidades.Banco.ctrPessoa;
import entidades.Pessoa;
import java.util.ArrayList;

/**
 *
 * @author Aluno
 */
public class Cliente extends Pessoa{
    private ArrayList<Venda> Compras = null;
    private ArrayList<Recebimento> Pagamentos = null;
    private String Senha;
    private Carrinho carrinho = null;

    public Cliente(String Nome, String RG, String CPF, String Endereco, String Telefone, String Email, String Senha) {
        super(Nome, RG, CPF, Endereco, Telefone, Email,2);
        this.Senha = Senha;
        setctrEntidade();
    }

    public Cliente() {
        super(0);
        setctrEntidade();
    }
    
    public ArrayList<Venda> getCompras() {
        return Compras;
    }

    public ArrayList<Recebimento> getPagamentos() {
        return Pagamentos;
    }

    public void setCompras(ArrayList<Venda> Compras) {
        this.Compras = Compras;
    }

    public void setPagamentos(ArrayList<Recebimento> Pagamentos) {
        this.Pagamentos = Pagamentos;
    }
    
    public Carrinho getCarrinho() {
        if(carrinho == null){
            carrinho = new Carrinho();
        }
        return carrinho;
    }

    public void setCarrinho(Carrinho carrinho) {
        this.carrinho = carrinho;
    }

    public String getSenha() {
        return Senha;
    }

    public void setSenha(String Senha) {
        this.Senha = Senha;
    }
    
   @Override
   public ArrayList<Indice> getIndices() {
        if(Indices == null){
            Indices = super.getIndices();
            Indices.add(new Indice(Senha, "cli_senha", "str", "Senha"));
        }else{
            Indices = super.getIndices();
            Indices.get(Indices.size()-1).setAtributo(Senha);
        }
        return Indices;
    }

    @Override
    public void setCtrEntidadeWeb() {
        Tipo = "cliente";
    }
   
   
}
