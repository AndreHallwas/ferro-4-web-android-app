/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entidades;

import Controladora.CtrlCategoria;
import entidades.Banco.Indice;
import entidades.Banco.ctrCategoria;
import entidades.Banco.ctrProduto;
import entidades.ControledeEntidade;
import java.awt.image.BufferedImage;
import java.util.ArrayList;

/**
 *
 * @author Aluno
 */
public class Produto extends ControledeEntidade{
    private String Codigo;
    private String Nome;
    private String Descricao;
    private Estoque estoque;
    private Categoria categoria;
    private String Valor;
    private BufferedImage Imagem;

    public Produto(String Codigo, String Nome, String Descricao, Categoria categoria, String Valor) {
        this.Codigo = Codigo;
        this.Nome = Nome;
        this.Descricao = Descricao;
        this.categoria = categoria;
        this.Valor = Valor;
        setctrEntidade();
    }
    
    public Produto(String Codigo, String Nome, String Descricao, Estoque estoque, Categoria categoria, String Valor) {
        this.Codigo = Codigo;
        this.Nome = Nome;
        this.Descricao = Descricao;
        this.estoque = estoque;
        this.categoria = categoria;
        this.Valor = Valor;
        setctrEntidade();
    }

    public Produto() {
        setctrEntidade();
    }
    
    public void setValor(double Valor) {
        this.Valor = Valor+"";
    }
    
    public String getCodigo() {
        return Codigo;
    }

    public String getNome() {
        return Nome;
    }

    public String getDescricao() {
        return Descricao;
    }

    public Estoque getEstoque() {
        return estoque;
    }

    public Categoria getCategoria() {
        return categoria;
    }

    public void setCodigo(String Codigo) {
        this.Codigo = Codigo;
    }
    
    public void setCodigo(int Codigo) {
        this.Codigo = Codigo+"";
    }

    public void setNome(String Nome) {
        this.Nome = Nome;
    }

    public void setDescricao(String Descricao) {
        this.Descricao = Descricao;
    }

    public void setEstoque(Estoque estoque) {
        this.estoque = estoque;
    }

    public void setCategoria(Categoria categoria) {
        this.categoria = categoria;
    }
    
    public void setCategoria(int categoria) {
        this.categoria = (Categoria) CtrlCategoria.getEntidadeE(categoria+"", new Categoria());
    }

    public String getValor() {
        return Valor;
    }

    public void setValor(String Valor) {
        this.Valor = Valor;
    }

    @Override
    void setctrEntidade() {
        Banco = Banco == null ? new ctrProduto(this) : Banco;
        setCtrEntidadeWeb();
    }

    @Override
    public ArrayList<Indice> getIndices() {
        if(Indices == null){
            Indices = new ArrayList();
            Indices.add(new Indice(Codigo, "prod_cod","int",'1',"Código","Codigo"));
            Indices.add(new Indice(Nome, "prod_nome","str","Nome"));
            Indices.add(new Indice(Valor, "prod_valor","double","Valor"));
            Indices.add(new Indice(Descricao, "prod_desc","str","Descrição","Descricao"));
            Indices.add(new Indice(categoria != null ? categoria.getCodigo() : null, "cat_cod","int","Categoria"));
            Indices.add(new Indice(categoria != null ? categoria.getNome() : null, "cat_cod","int",'5',"Categoria"));
            /*Indices.add(new Indice(estoque.getQuant(), "prod_estoque","int","Estoque"));*/
            Indices.add(new Indice(Imagem, "prod_foto","img",'5',"Imagem"));
        }else{
            Indices.get(0).setAtributo(Codigo);
            Indices.get(1).setAtributo(Nome);
            Indices.get(2).setAtributo(Valor);
            Indices.get(3).setAtributo(Descricao);
            Indices.get(4).setAtributo(categoria != null ? categoria.getCodigo() : null);
            Indices.get(5).setAtributo(categoria != null ? categoria.getNome() : null);
            Indices.get(6).setAtributo(Imagem);
        }
        return Indices;
    }

    @Override
    public void setCtrEntidadeWeb() {
        Tipo = "produto";
    }
    
    
}
