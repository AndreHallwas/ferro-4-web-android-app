/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entidades;

import entidades.Banco.Indice;
import entidades.Pessoa;
import java.util.ArrayList;

/**
 *
 * @author Aluno
 */
public class Usuario extends Pessoa {

    protected String Login;
    protected String Senha;
    protected double Salario;

    public Usuario(String Login, String Senha, String Salario, String Nome, String RG, String CPF, String Endereco, String Telefone, String Email) {
        super(Nome, RG, CPF, Endereco, Telefone, Email, 1);
        this.Login = Login;
        this.Senha = Senha;
        this.Salario = Salario == null || Salario.isEmpty() ? 0 : Double.parseDouble(Salario);
        setctrEntidade();
    }

    public Usuario(String Login, String Senha, double Salario, String Nome, String RG, String CPF, String Endereco, String Telefone, String Email) {
        super(Nome, RG, CPF, Endereco, Telefone, Email, 1);
        this.Login = Login;
        this.Senha = Senha;
        this.Salario = Salario;
        setctrEntidade();
    }

    public Usuario() {
        super(1);
        setctrEntidade();
    }

    public String getLogin() {
        return Login;
    }

    public String getSenha() {
        return Senha;
    }

    public double getSalario() {
        return Salario;
    }

    public void setLogin(String Login) {
        this.Login = Login;
    }

    public void setSenha(String Senha) {
        this.Senha = Senha;
    }

    public void setSalario(double Salario) {
        this.Salario = Salario;
    }

    @Override
    public ArrayList<Indice> getIndices() {
        if (Indices == null) {
            Indices = new ArrayList();
            Indices.add(new Indice(Login, "usr_login", "str", '1', "Login"));
            Indices.add(new Indice(Senha, "usr_senha", "str", "Senha"));
            Indices.add(new Indice(Salario, "usr_salario", "double", "Salário", "Salario"));
            Indices.add(new Indice(Nome, "usr_nome", "str", "Nome"));
            Indices.add(new Indice(RG, "usr_rg", "str", "RG"));
            Indices.add(new Indice(CPF, "usr_cpf", "str", "CPF"));
            Indices.add(new Indice(Endereco, "usr_endereco", "str", "Endereço", "Endereco"));
            Indices.add(new Indice(Telefone, "usr_telefone", "str", "Telefone"));
            Indices.add(new Indice(Email, "usr_email", "str", "E-mail", "Email"));
        }else{
            Indices.get(0).setAtributo(Login);
            Indices.get(1).setAtributo(Senha);
            Indices.get(2).setAtributo(Salario);
            Indices.get(3).setAtributo(Nome);
            Indices.get(4).setAtributo(RG);
            Indices.get(5).setAtributo(CPF);
            Indices.get(6).setAtributo(Endereco);
            Indices.get(7).setAtributo(Telefone);
            Indices.get(8).setAtributo(Email);
        }
        return Indices;
    }

    @Override
    public void setCtrEntidadeWeb() {
        Tipo = "usuario";
    }
    
    
}
