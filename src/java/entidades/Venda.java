/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entidades;

import Controladora.CtrlPessoa;
import entidades.Banco.Indice;
import entidades.Banco.ctrVenda;
import java.time.LocalDate;
import java.util.ArrayList;

/**
 *
 * @author Aluno
 */
public class Venda extends ControledeEntidade {

    private String Codigo;
    private Cliente cliente;
    private LocalDate Data;
    private ArrayList<Recebimento> recebimentos = null;
    private ArrayList<ProdutosVenda> ProdutosdaVenda = null;

    public Venda(String Codigo, Cliente cliente, LocalDate Data) {
        this.Codigo = Codigo;
        this.cliente = cliente;
        setData(LocalDate.now());
        this.Data = Data;
        setctrEntidade();
    }

    public Venda() {
        setData(LocalDate.now());
        setctrEntidade();
    }

    public Venda(Cliente cliente) {
        this.cliente = cliente;
        setData(LocalDate.now());
        setctrEntidade();
    }

    public boolean Inicia() {
        boolean flag = false;
        if (Codigo == null) {
            flag = add();
            Codigo = Integer.toString(Utils.Banco.getCon().getMaxPK("venda", "ven_cod"));
        }
        return flag;
    }

    public ArrayList<ProdutosVenda> getProdutosdaVenda() {
        return ProdutosdaVenda;
    }

    public void setProdutosdaVenda(ArrayList<Entidade> ProdutosdaVenda) {
        this.ProdutosdaVenda = new ArrayList();
        for (Entidade entidade : ProdutosdaVenda) {
            ((ProdutosVenda) entidade).setCompra(this);
            this.ProdutosdaVenda.add((ProdutosVenda) entidade);
        }
    }

    public LocalDate getData() {
        return Data == null ? LocalDate.now() : Data;
    }

    public void setData(LocalDate Data) {
        this.Data = Data;
    }

    public String getCodigo() {
        return Codigo;
    }

    public Cliente getCliente() {
        return cliente;
    }

    public ArrayList<Recebimento> getRecebimentos() {
        return recebimentos;
    }

    public void setCodigo(String Codigo) {
        this.Codigo = Codigo;
    }

    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }
    
    public void setCPF(String cliente) {
        this.cliente = (Cliente) CtrlPessoa.getEntidadeE(cliente+"", new Cliente());
    }

    public void setRecebimentos(ArrayList<Entidade> recebimentos) {
        this.recebimentos = new ArrayList();
        for (Entidade recebimento : recebimentos) {
            ((Recebimento) recebimento).setCompra(this);
            this.recebimentos.add((Recebimento) recebimento);
        }
    }

    public void setCodigo(int Codigo) {
        this.Codigo = Codigo + "";
    }

    @Override
    void setctrEntidade() {
        Banco = Banco == null ? new ctrVenda(this) : Banco;
        setCtrEntidadeWeb();
    }

    @Override
    public ArrayList<Indice> getIndices() {
        if (Indices == null) {
            Indices = new ArrayList();
            Indices.add(new Indice(Codigo, "ven_cod", "int", '1', "Código","Codigo"));
            Indices.add(new Indice(Data, "ven_data", "date", "Data"));
            Indices.add(new Indice(cliente != null ? cliente.getCPF() : null, "cli_cpf", "str", "CPF"));
        }else{
            Indices.get(0).setAtributo(Codigo);
            Indices.get(1).setAtributo(Data);
            Indices.get(2).setAtributo(cliente != null ? cliente.getCPF() : null);
        }
            
        return Indices;
    }

    @Override
    public String toHiperLine(int Parametro, String... param) {
        String conteudo = "";
        conteudo += super.toHiperLine(0, "-1"); //To change body of generated methods, choose Tools | Templates.
        Carrinho c = new Carrinho();
        for (ProdutosVenda produto : ProdutosdaVenda) {
            c.add(produto.getProduto(), Integer.parseInt(produto.getQuant()));
        }
        conteudo += " " + c.toHiperLine("-1");
        return conteudo;
    }
    
    

    @Override
    public void setCtrEntidadeWeb() {
        Tipo = "venda";
    }

}
