/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entidades;

import entidades.Banco.Indice;
import java.util.ArrayList;

/**
 *
 * @author Raizen
 */
public abstract class EntidadeWeb {

    protected ArrayList<Indice> Indices;
    protected String Tipo;

    public abstract ArrayList<Indice> getIndices();

    public abstract void setCtrEntidadeWeb();

    public String toHiperLineTableHeader(Indice indice) {
        return "<th>" + indice.getNome() + "</th>";
    }

    public String toHiperLineImage(String Indice) {
        /*<form method="POST" action="UpServlet" enctype="multipart/form-data" >
                Arquivo:
                <input type="file" name="file" id="file" /> <br/>
                Destino:
                <input type="text" value="tmp" name="destination"/>
                </br>
                <input type="submit" value="Upload" name="upload" id="upload" />
        </form>*/
        return "<input type='file' name='img" + Indice + "' id='img" + Indice + "' /> <br/>";
    }

    public String toHiperLineTableLine(Indice indice) {
        return "<td>" + ((indice.getAtributo() + "").isEmpty() ? "" : indice.getAtributo() + "") + "</td>";
    }

    public Indice getChave() {
        Indices = getIndices();
        int i = 0;
        while (i < Indices.size() && Indices.get(i).getChave() == '1') {
            i++;
        }
        return i < Indices.size() ? Indices.get(i) : new Indice(Tipo, Tipo, Tipo, Tipo);
    }

    public String toHiperLine(int Parametro, String... param) {
        setCtrEntidadeWeb();
        String string = "";
        String string2 = "";
        Indice chave = getChave();
        string2 += "<tbody><tr>";
        if (Parametro == 0) {
            string += "<thead><tr>";
            for (Indice indice : Indices) {
                string += toHiperLineTableHeader(indice);
                string2 += toHiperLineTableLine(indice);
                if (indice.getChave() == '1') {
                    chave = indice;
                }
            }
            string += "<th class='actions'>Ações</th>";
            string += "</tr></thead>";
        } else {
            for (Indice indice : Indices) {
                string2 += toHiperLineTableLine(indice);
                if (indice.getChave() == '1') {
                    chave = indice;
                }
            }
        }
        string2 += toHiperLineActions(chave, Tipo, param);
        string2 += "</tr></tbody>";
        return string + string2;

    }

    public String toHiperLineActions(Indice chave, String Tipo, String... Param) {
        String string = "";
        string += "<td class='actions'>";
        string += toHiperLineButtonCRUD(chave, Tipo, Param);
        string += "<input type='hidden' id='chave' value='" + ((chave.getAtributo() + "").isEmpty() ? "" : chave.getAtributo() + "") + "'/>";
        string += "</td>";
        return string;
    }

    public String toHiperLineButtonCRUD(Indice chave, String Tipo, String... Param) {
        String string = "";
        if (Param != null && Param.length > 0) {
            if (Param[0].equals("1")) {
                string += "<a class='btn btn-success btn-xs' href='CtrlInterface?acao=painelCentralView&tipo=" + Tipo + "&param=visualizar&chave=" + chave.getAtributo().toString() + "'>Visualizar</a>"
                        + "<a class='btn btn-warning btn-xs' href='CtrlInterface?acao=venda&tipo=" + Tipo + "&param=adicionar&chave=" + chave.getAtributo().toString() + "'>Adicionar</a>";
            } else if (Param[0].equals("-1")) {
                return string;
            } else if (Param[0].equals("2")) {
                string += "<a class='btn btn-success btn-xs' href='CtrlInterface?acao=painelCentralView&tipo=" + Tipo + "&param=visualizar&chave=" + chave.getAtributo().toString() + "'>Visualizar</a>"
                        + "<a class='btn btn-erro btn-xs' href='CtrlInterface?acao=carrinho&tipo=" + Tipo + "&param=excluir&chave=" + chave.getAtributo().toString() + "'>Excluir</a>";
            }
        } else {
            string += "<a class='btn btn-success btn-xs' href='CtrlInterface?acao=painelCentralView&tipo=" + Tipo + "&param=visualizar&chave=" + chave.getAtributo().toString() + "'>Visualizar</a>"
                    + "<a class='btn btn-warning btn-xs' href='CtrlInterface?acao=painelCampos&tipo=" + Tipo + "&param=alterar&chave=" + chave.getAtributo().toString() + "'>Editar</a>"
                    + "<a class='btn btn-danger btn-xs' data-toggle='modal' data-target='#delete-modal'>Excluir</a>";
        }

        return string;
    }

    public String toHiperLineColumn(String Campo, int ColType, String Tipo) {
        return "<div class='form-group col-md-" + ColType + "'><label for='" + Campo + "'>" + Campo + "</label>"
                + "<input type='" + Tipo + "' class='form-control' name='" + Campo + "' placeholder='Digite o valor'></div>";
    }

    public String toHiperLineColumn(String Campo, int ColType, String Value, String Tipo) {
        return "<div class='form-group col-md-" + ColType + "'><label for='" + Campo + "'>" + Campo + "</label>"
                + "<input type='" + Tipo + "' class='form-control' name='" + Campo + "' value='" + Value + "' placeholder='Digite o valor'></div>";
    }

    public String toHiperLineColumns(int chave, String... Params) {
        String string = "<div class='row'>";
        ArrayList<Indice> Indices = getIndices();

        if (chave == 1) {
            for (Indice indice : Indices) {
                string += toHiperLineColumn(indice.getCampo(), 4, "text");
            }
        } else {
            for (Indice indice : Indices) {
                string += toHiperLineColumn(indice.getCampo(), 4, (indice.getAtributo() == null ? ""
                        : indice.getAtributo().toString()), "text");
            }
        }
        if (Params != null && Params[0] != null && Params[0].equalsIgnoreCase("Img")) {
            string += toHiperLineColumn("Imagem 1", 4, "file");
            string += toHiperLineColumn("Imagem 2", 4, "file");
            string += toHiperLineColumn("Imagem 3", 4, "file");
        }

        string += "</div>";
        return string;
    }

    public String toHiperLineViewColumn(String Campo, String Informacao) {
        return "<div class='col-md-4'>" + "<p><strong>" + Campo + "</strong></p>"
                + "<p>" + Informacao + "</p>" + "</div>";
    }

    public String toHiperLineViewColumns() {
        String string = "<div class='row'>";
        ArrayList<Indice> Indices = getIndices();
        string += "<h3 class='page-header'>" + Tipo + "</h3>"
                + "<div class='row'>";
        for (Indice indice : Indices) {
            string += toHiperLineViewColumn(indice.getNome(), (indice.getAtributo() == null ? ""
                    : indice.getAtributo().toString()));
        }
        string += "</div>";
        return string;
    }
}
