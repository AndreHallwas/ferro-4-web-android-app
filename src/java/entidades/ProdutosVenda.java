/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entidades;

import Controladora.CtrlProduto;
import Controladora.CtrlVenda;
import entidades.Banco.Indice;
import entidades.Banco.ctrCategoria;
import entidades.Banco.ctrProdutosVenda;
import entidades.ControledeEntidade;
import java.util.ArrayList;

/**
 *
 * @author Aluno
 */
public class ProdutosVenda extends ControledeEntidade{
    private Venda compra;
    private Produto produto;
    private String Quant;

    public ProdutosVenda() {
        setctrEntidade();
    }
    
    public ProdutosVenda(Venda compra, Produto produto, String Quant) {
        this.compra = compra;
        this.produto = produto;
        this.Quant = Quant;
        setctrEntidade();
    }

    public Venda getCompra() {
        return compra;
    }

    public Produto getProduto() {
        return produto;
    }

    public String getQuant() {
        return Quant;
    }

    public void setCompra(Venda compra) {
        this.compra = compra;
    }

    public void setProduto(Produto produto) {
        this.produto = produto;
    }

    public void setQuant(String Quant) {
        this.Quant = Quant;
    }
    
    public void setQuantidade(int Quant) {
        this.Quant = Quant+"";
    }
    
    public void setCompra(int Venda){
        this.compra = /*(Venda) CtrlVenda.getEntidadeE(Venda+"", new Venda());*/ null;
    }
    
    public void setProduto(int Produto){
        this.produto = (Produto) CtrlProduto.getEntidadeE(Produto+"", new Produto());
    }

    @Override
    void setctrEntidade() {
        Banco = Banco == null ? new ctrProdutosVenda(this) : Banco;
        setCtrEntidadeWeb();
    }

    @Override
    public ArrayList<Indice> getIndices() {
        if(Indices == null){
            Indices = new ArrayList();
            Indices.add(new Indice(produto != null ? produto.getCodigo() : produto, "prod_cod","int",'1',"Produto"));
            Indices.add(new Indice(compra != null ? compra.getCodigo() : compra, "ven_cod","int",'1',"Compra"));
            Indices.add(new Indice(Quant, "prod_quant","int","Quantidade"));
        }else{
            Indices.get(0).setAtributo(produto != null ? produto.getCodigo() : produto);
            Indices.get(1).setAtributo(compra != null ? compra.getCodigo() : compra);
            Indices.get(2).setAtributo(Quant);
        }
        return Indices;
    }

    @Override
    public void setCtrEntidadeWeb() {
        Tipo = "produtosvenda";
    }
    
    
}
