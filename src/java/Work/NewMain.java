/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Work;

import Controladora.CtrlVenda;
import Utils.Banco;
import entidades.Banco.ctrPessoa;
import entidades.Categoria;
import entidades.Cliente;
import entidades.Estoque;
import entidades.Pessoa;
import entidades.Produto;
import entidades.ProdutosVenda;
import entidades.Usuario;
import java.lang.reflect.Method;
import java.util.logging.Level;
import java.util.logging.Logger;
import jdk.nashorn.internal.objects.NativeFunction;

/**
 *
 * @author Raizen
 */
public class NewMain {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        String Nada = "nada3";
        Banco.conectar();
        Pessoa c;
        /*
        c = new Usuario(Nada, "Trolei", 1, Nada, Nada, Nada, Nada, Nada, Nada);
        c.add();
        c.setEmail(Nada+"3");
        c.altera();
        c.get("");
        c.remove();*/
       
        c = new Cliente(Nada, Nada, Nada, Nada, Nada, Nada, Nada);
        c.add();
        ///c.setEmail(Nada+"3");
        ///c.altera();
        ///c.get("");
        ////c.remove();
        
        Categoria ca = new Categoria(null, Nada, Nada);
        ca.add();
        ca.setCodigo(Banco.getCon().getMaxPK("categoria", "cat_cod")+"");
        ////ca.setDescricao(Nada+"324165654");
        ///ca.altera();
        ///ca.get("");
        ///ca.remove();
        
        Produto p = new Produto(null, Nada, Nada, ca, "50");
        p.add();
        p.setCodigo(Banco.getCon().getMaxPK("produto", "prod_cod"));
        /////p.setDescricao(Nada+"3");
        /////p.altera();
        ////p.get("");
        ///p.remove();
        
        /*ProdutosVenda pv = new ProdutosVenda();
        pv.setProduto(12);*/
        
        ((Cliente)c).getCarrinho().add(p, 5);
        ((Cliente)c).getCarrinho().add(p, 15);
        CtrlVenda cv = new CtrlVenda();
        /*cv.vender(((Cliente)c), 2);*/
        /////cv.vender(((Cliente)c), 12);
        cv.pesquisar("");
        return;
    }

    public static void function(Object c, String function, String a) {
        Method getNameMethod;
        String name;
        try {
            ///////Class<?> usuario = Class.forName(dogClassName); // convert string classname to class
            ///////Object usr = usuario.newInstance(); // invoke empty constructor
            getNameMethod = c.getClass().getMethod(function, a.getClass());
            name = (String) getNameMethod.invoke(c);
            /*c.getClass().getMethod("get" + "Login").invoke(c, (Object) new String());*/
        } catch (Exception ex) {
        }

    }

    public static void function1(String nomeClasse, Object obj, String Metodo, Object... args) {

        Class[] params = {};
        if (args.length > 0) {
            params = new Class[args.length];
            for (int i = 0; i < args.length; i++) {
                params[i] = args[i].getClass();/*Integer.TYPE*/
            }
        }
        try {
            
            try{
                Class cls = Class.forName(nomeClasse);
                obj = cls.newInstance();
            }catch(Exception e){}
            
            Method method = obj.getClass().getDeclaredMethod(Metodo, params);
            method.invoke(obj, args);

        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public static void function2(String... args) {

        //no paramater
        Class noparams[] = {};

        //String parameter
        Class[] paramString = new Class[1];
        paramString[0] = String.class;

        //int parameter
        Class[] paramInt = new Class[1];
        paramInt[0] = Integer.TYPE;

        try {
            //load the AppTest at runtime
            Class cls = Class.forName("com.mkyong.reflection.AppTest");
            Object obj = cls.newInstance();

            //call the printIt method
            Method method = cls.getDeclaredMethod("printIt", noparams);
            method.invoke(obj, null);

            //call the printItString method, pass a String param
            method = cls.getDeclaredMethod("printItString", paramString);
            method.invoke(obj, new String("mkyong"));

            //call the printItInt method, pass a int param
            method = cls.getDeclaredMethod("printItInt", paramInt);
            method.invoke(obj, 123);

            //call the setCounter method, pass a int param
            method = cls.getDeclaredMethod("setCounter", paramInt);
            method.invoke(obj, 999);

            //call the printCounter method
            method = cls.getDeclaredMethod("printCounter", noparams);
            method.invoke(obj, null);

        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

}
