/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controladora;

import entidades.Entidade;
import entidades.EntidadeWeb;
import entidades.Produto;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.nio.channels.FileChannel;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.Servlet;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;

/**
 *
 * @author Raizen
 */
@MultipartConfig(
        location = "/",
        fileSizeThreshold = 1024 * 1024, // 1 MB
        maxFileSize = 1024 * 1024 * 100, // 100 MB 
        maxRequestSize = 1024 * 1024 * 10 * 10 // 100     MB
)
@WebServlet(name = "CtrlEntidade", urlPatterns = {"/CtrlEntidade"})
public abstract class CtrlEntidade extends HttpServlet {

    protected static Entidade p;
    protected String acao;
    protected String id;
    protected String erro = "";
    protected String[] Img = null;
    public static String ServletContext = "";

    public abstract void getParametros(HttpServletRequest request, HttpServletResponse response);

    public abstract void getEntidade(HttpServletRequest request, HttpServletResponse response);

    public static String getEntidade(String filtro, Entidade p, String... Params) {
        String conteudo = "";
        ArrayList<Entidade> lista = p.get(filtro);
        for (int i = 0; i < lista.size(); i++) {
            conteudo += ((EntidadeWeb) lista.get(i)).toHiperLine(i, Params);
        }
        return conteudo;
    }

    public static Entidade getEntidadeE(String filtro, Entidade p) {
        ArrayList<Entidade> entidades = p.get(filtro);
        return entidades.isEmpty() ? null : entidades.get(0);
    }

    public boolean Acao(HttpServletRequest request, HttpServletResponse response) throws IOException {
        ServletContext = getServletContext().getRealPath("/");
        if (p != null) {
            switch (acao) {
                case "Gravar":
                    if (Integer.parseInt(id) == 0) {
                        if (!p.add()) {
                            erro = "Erro ao Adicionar";
                        } else {
                            if (p instanceof Produto) {
                                processarImagem(request, response, ((Produto) p).getNome() + " 1", "Imagem 1");
                                processarImagem(request, response, ((Produto) p).getNome() + " 2", "Imagem 2");
                                processarImagem(request, response, ((Produto) p).getNome() + " 3", "Imagem 3");
                            }
                        }
                    } else {
                        if (!p.altera()) {
                            erro = "Erro ao Alterar";
                        } else {
                            if (p instanceof Produto) {
                                processarImagem(request, response, ((Produto) p).getNome() + " 1", "Imagem 1");
                                processarImagem(request, response, ((Produto) p).getNome() + " 2", "Imagem 2");
                                processarImagem(request, response, ((Produto) p).getNome() + " 3", "Imagem 3");
                            }
                        }
                    }
                    break;
                case "Apagar":
                    if (!p.remove()) {
                        erro = "Erro ao Excluir";
                    }
                    break;
                case "Alterar":
                    try {
                        p = p.get(id).get(0);
                    } catch (Exception ex) {
                        erro = "Erro ao Alterar";
                    }
                    break;
                case "Filtrar":
                    String conteudo = "";
                    ArrayList<Entidade> lista = p.get(request.getParameter("filtro"));
                    for (int i = 0; i < lista.size(); i++) {
                        conteudo += ((EntidadeWeb) lista.get(i)).toHiperLine(i);
                    }
                    response.setContentType("text/html;charset=utf-8");
                    response.getWriter().println(conteudo);
                    return true;
                case "Carrinho":
                    response.sendRedirect("carrinho.jsp");
                    return true;
            }
            response.sendRedirect("list.jsp");
            return true;
        }
        return false;
    }

    protected void processarImagem(HttpServletRequest request, HttpServletResponse response, String nome, String envio) {

        // lê a pasta de destino
        Part filePart;
        try {
            filePart = request.getPart(envio); // Lê o arquivo de upload
            OutputStream out = null;
            InputStream filecontent = null;
            try {  //criando a pasta
                File fpasta = new File(CtrlInterface.ServletContext + "img");
                fpasta.mkdir();
                File novo = new File(fpasta.getAbsolutePath() + "/" + nome + ".jpg");
                out = new FileOutputStream(novo);
                filecontent = filePart.getInputStream();
                int read = 0;
                byte[] bytes = new byte[1024];
                while ((read = filecontent.read(bytes)) != -1) {
                    out.write(bytes, 0, read);
                }
                out.close();
                filecontent.close();
                String CaminhoReal = new File(new File(new File(fpasta.getAbsolutePath()).getParent()).getParent()).getParent();
                File backup = new File(CaminhoReal+"/web/img" + "/" + nome + ".jpg");
                copyFile(novo, backup);/*É necessário pois quando o arquivo é salvo no servlet 
                context ele é salvo na pasta build, porem apos recompilar o projeto a mesma é apagada. 
                então é necessário salvar na pasta web, porque assim quando é feito o rebuild ele será re-copiado para a build.*/
            } catch (Exception fne) {
            }
        } catch (Exception ex) {
            Logger.getLogger(CtrlEntidade.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public static void copyFile(File source, File destination) throws IOException {
        if (destination.exists()) {
            destination.delete();
        }
        FileChannel sourceChannel = null;
        FileChannel destinationChannel = null;
        try {
            sourceChannel = new FileInputStream(source).getChannel();
            destinationChannel = new FileOutputStream(destination).getChannel();
            sourceChannel.transferTo(0, sourceChannel.size(),
                    destinationChannel);
        } finally {
            if (sourceChannel != null && sourceChannel.isOpen()) {
                sourceChannel.close();
            }
            if (destinationChannel != null && destinationChannel.isOpen()) {
                destinationChannel.close();
            }
        }
    }

    protected void processarImagem(HttpServletRequest request, HttpServletResponse response) throws Exception {
        // lê a pasta de destino
        String pasta = request.getParameter("destination");
        Part filePart = request.getPart("file");  // Lê o arquivo de upload
        String fileName = filePart.getSubmittedFileName();

        OutputStream out = null;
        InputStream filecontent = null;
        PrintWriter writer = response.getWriter();
        try {  //criando a pasta
            File fpasta = new File(getServletContext().getRealPath("/") + "/" + pasta);
            fpasta.mkdir();
            out = new FileOutputStream(new File(fpasta.getAbsolutePath() + "/" + "zé.ppt"));
            filecontent = filePart.getInputStream();
            int read = 0;
            byte[] bytes = new byte[1024];
            while ((read = filecontent.read(bytes)) != -1) {
                out.write(bytes, 0, read);
            }
            writer.println("Novo arquivo " + fileName + " criado na pasta " + pasta);
            out.close();
            filecontent.close();
            writer.close();
        } catch (Exception fne) {
            writer.println("Erro ao receber o arquivo");
            writer.println("<br/> ERRO: " + fne.getMessage());
        }

    }

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        getParametros(request, response);
        getEntidade(request, response);
        if (!Acao(request, response)) {
            response.sendRedirect("index.jsp");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
