/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controladora;

import static Controladora.CtrlEntidade.p;
import Utils.Mensagem;
import entidades.Entidade;
import entidades.Venda;
import entidades.Recebimento;
import java.time.LocalDate;
import java.util.ArrayList;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Aluno
 */
@MultipartConfig(
    location="/", 
    fileSizeThreshold=1024*1024,    // 1 MB
    maxFileSize=1024*1024*100,       // 100 MB 
    maxRequestSize=1024*1024*10*10    // 100     MB
)
@WebServlet(name = "CtrlRecebimento", urlPatterns = {"/CtrlRecebimento"})
public class CtrlRecebimento extends CtrlEntidade {

    public String Codigo;
    public Venda compra;
    public LocalDate Vencimento;
    public LocalDate Pagamento;
    public String NumerodaParcela;
    public String Valor;
    public String Situacao;
    private String chave;
    private String tipo;

    @Override
    public void getParametros(HttpServletRequest request, HttpServletResponse response) {
        acao = request.getParameter("acao");
        id = request.getParameter("id");
        tipo = request.getParameter("tipo");
        chave = request.getParameter("chave");
        compra = (Venda) CtrlVenda.getEntidadeE(request.getParameter("compra"), new Venda());
        Codigo = request.getParameter("codigo");
        NumerodaParcela = request.getParameter("numerodaparcela");
        Valor = request.getParameter("valor");
        Situacao = request.getParameter("situacao");
        Codigo = request.getParameter("codigo");
        Vencimento = LocalDate.parse(request.getParameter("vencimento"));
        Pagamento = LocalDate.parse(request.getParameter("recebimento"));
    }

    @Override
    public void getEntidade(HttpServletRequest request, HttpServletResponse response) {
        if (tipo != null && !tipo.isEmpty()) {
            if (tipo.equalsIgnoreCase("Recebimento")) {
                if (chave == null || chave.isEmpty()) {
                    p = new Recebimento(Codigo, compra, Vencimento, Pagamento, NumerodaParcela, Valor, Situacao);
                } else {
                    p = new Recebimento(chave, compra, Vencimento, Pagamento, NumerodaParcela, Valor, Situacao);
                }
            }
        }
    }

    public boolean gerar(Venda venda, LocalDate Vencimento, int Parcelas, int Valor) {
        boolean flag = true;
        ArrayList<Entidade> recebimentos = inicia(Parcelas, Valor, venda);
        Vencimento = LocalDate.now();
        /*
        for (Entidade recebimento : recebimentos) {
            flag = flag && recebimento.add();
        }*/

        venda.setRecebimentos(recebimentos);
        return flag;
    }

    protected ArrayList<Entidade> inicia(int Parcelas, int Valor, Venda venda) {
        ArrayList<Entidade> recebimentos = new ArrayList();

        if (Parcelas > 0) {
            Valor = Valor / Parcelas;

            for (int i = 0; i < Parcelas; i++) {
                recebimentos.add(new Recebimento(venda, Vencimento, i + "", Valor + ""));
            }

        } else {
            recebimentos.add(new Recebimento(venda, Vencimento, 1 + "", Valor + ""));
        }

        return recebimentos;
    }

    public boolean receber(String Codigo) {
        boolean flag = true;
        try {
            ArrayList<Entidade> recebimentos = pesquisar(Codigo);
            for (Entidade recebimento : recebimentos) {
                ((Recebimento) recebimento).setPagamento(LocalDate.now());
                ((Recebimento) recebimento).setSituacao("Pago");
                flag = flag && recebimento.altera();
            }
        } catch (Exception ex) {
            Mensagem.ExibirException(ex, "Erro ao Receber");
            flag = false;
        }
        return flag;
    }

    public ArrayList<Entidade> pesquisar(String Filtro) {
        Recebimento recebimento = new Recebimento();
        return recebimento.get(Filtro);
    }
}
