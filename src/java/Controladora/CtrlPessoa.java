/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controladora;

import static Controladora.CtrlEntidade.p;
import entidades.Cliente;
import entidades.Produto;
import entidades.Usuario;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Raizen
 */
@MultipartConfig(
    location="/", 
    fileSizeThreshold=1024*1024,    // 1 MB
    maxFileSize=1024*1024*100,       // 100 MB 
    maxRequestSize=1024*1024*10*10    // 100     MB
)
public class CtrlPessoa extends CtrlEntidade{
    
    private String tipo;
    private String chave;
    private String nome;
    private String rg;
    private String cpf;
    private String endereco;
    private String telefone;
    private String email;
    private String login;
    private String senha;
    private String salario;
    private String produto;
    private String produtoq;

    @Override
    public void getParametros(HttpServletRequest request, HttpServletResponse response) {
        acao = request.getParameter("acao");
        id = request.getParameter("id");
        tipo = request.getParameter("tipo");
        chave = request.getParameter("chave");
        nome = request.getParameter("Nome");
        rg = request.getParameter("RG");
        cpf = request.getParameter("CPF");
        endereco = request.getParameter("Endereço");
        telefone = request.getParameter("Telefone");
        email = request.getParameter("E-mail");
        senha = request.getParameter("Senha");
        produto = request.getParameter("produto");
        produtoq = request.getParameter("produtoq");
        login = "";
        salario = "";
    }

    @Override
    public void getEntidade(HttpServletRequest request, HttpServletResponse response) {
        if (tipo != null && !tipo.isEmpty()) {
            if (tipo.equalsIgnoreCase("Usuario")) {
                if (chave == null || chave.isEmpty()) {
                    login = request.getParameter("Login");
                    salario = request.getParameter("Salário");
                    p = new Usuario(login, senha, salario, nome, rg, cpf, endereco, telefone, email);
                } else {
                    p = new Usuario(chave, senha, salario, nome, rg, cpf, endereco, telefone, email);
                }
            } else {
                Produto prod;
                if (chave == null || chave.isEmpty()) {
                    p = new Cliente(nome, rg, cpf, endereco, telefone, email, senha);
                }else{
                    p = new Cliente(nome, rg, chave, endereco, telefone, email, senha);
                }
                if(produto != null && !produto.isEmpty()){
                    prod = (Produto) CtrlProduto.getEntidadeE(produto, new Produto());
                    ((Cliente)p).getCarrinho().add(prod, Integer.parseInt(produtoq));
                }
            }
        }
    }
}
