/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controladora;

import static Controladora.CtrlEntidade.p;
import Utils.Banco;
import Utils.Mensagem;
import entidades.Carrinho;
import entidades.Categoria;
import entidades.Cliente;
import entidades.Entidade;
import entidades.Venda;
import entidades.Produto;
import entidades.ProdutosVenda;
import entidades.Recebimento;
import java.time.LocalDate;
import java.util.ArrayList;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Aluno
 */
@MultipartConfig(
    location="/", 
    fileSizeThreshold=1024*1024,    // 1 MB
    maxFileSize=1024*1024*100,       // 100 MB 
    maxRequestSize=1024*1024*10*10    // 100     MB
)
@WebServlet(name = "CtrlVenda", urlPatterns = {"/CtrlVenda"})
public class CtrlVenda extends CtrlEntidade {

    private String Codigo;
    private Cliente cliente;
    private Produto produto;
    private LocalDate Data;
    private String chave;
    private String tipo;
    private int Valor;

    @Override
    public void getParametros(HttpServletRequest request, HttpServletResponse response) {
        acao = request.getParameter("acao");
        id = request.getParameter("id");
        tipo = request.getParameter("tipo");
        chave = request.getParameter("chave");
        cliente = (Cliente) CtrlPessoa.getEntidadeE(request.getParameter("cliente"), new Cliente());
        produto = (Produto) CtrlProduto.getEntidadeE(request.getParameter("produto"), new Produto());
        /*
        *Produtos da Compra;
        *Recebimentos Gerados;
        */
        Codigo = request.getParameter("codigo");
        Data = request.getParameter("data") != null ?LocalDate.parse(request.getParameter("data")) : null;
    }

    @Override
    public void getEntidade(HttpServletRequest request, HttpServletResponse response) {
        if (tipo != null && !tipo.isEmpty()) {
            if (tipo.equalsIgnoreCase("Compra")) {
                if (chave == null || chave.isEmpty()) {
                    p = new Venda(Codigo, cliente, Data);
                } else {
                    p = new Venda(chave, cliente, Data);
                }
            }
        }
    }
    
    public boolean vender(Cliente cliente, int Parcelas){
        boolean flag = true;
        Venda venda = new Venda(cliente);
        /*flag = venda.Inicia();*/
        Valor = 0;
        CtrlRecebimento recebimento = new CtrlRecebimento();
        
        flag = flag && geraProdutosVenda(venda, cliente.getCarrinho(), flag);
        flag = flag && recebimento.gerar(venda, LocalDate.now(), Parcelas, Valor);
        
        venda.add();
        
        return flag;
    }
    
    public boolean geraProdutosVenda(Venda venda, Carrinho Itens, boolean flag){
        
        ArrayList<Carrinho.Itens> itens = Itens.get();
        ArrayList<Entidade> produtos = new ArrayList();
        
        for (Carrinho.Itens item : itens) {
            produtos.add(new ProdutosVenda(venda, item.getProduto(), item.getQuantidade()+""));
            Valor += item.getValor()*item.getQuantidade();
        }
        /*
        for (Entidade produto : produtos) {
            flag = flag && produto.add();
        }*/
        
        venda.setProdutosdaVenda(produtos);
        return flag;
    }
    
    public boolean cancelar(String Codigo){
        Venda venda = new Venda();
        try{
            return venda.get(Codigo).get(0).remove();
        }catch(Exception ex){
            Mensagem.ExibirException(ex, "Não foi Possivel Cancelar a Venda");
        }
        return false;
    }
    
    public ArrayList<Entidade> pesquisar(String Filtro){
        Venda venda = new Venda();
        return venda.get(Filtro);
    }
}
