/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controladora;

import static Controladora.CtrlEntidade.p;
import entidades.Categoria;
import entidades.Cliente;
import entidades.Usuario;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Aluno
 */
@MultipartConfig(
    location="/", 
    fileSizeThreshold=1024*1024,    // 1 MB
    maxFileSize=1024*1024*100,       // 100 MB 
    maxRequestSize=1024*1024*10*10    // 100     MB
)
@WebServlet(name = "CtrlCategoria", urlPatterns = {"/CtrlCategoria"})
public class CtrlCategoria extends CtrlEntidade{

    private String Codigo;
    private String Nome;
    private String Descricao;
    private String tipo;
    private String chave;
    
    @Override
    public void getParametros(HttpServletRequest request, HttpServletResponse response) {
        acao = request.getParameter("acao");
        id = request.getParameter("id");
        tipo = request.getParameter("tipo");
        chave = request.getParameter("chave");
        Nome = request.getParameter("Nome");
        Codigo = request.getParameter("Codigo");
        Descricao = request.getParameter("Descricao");
    }

    @Override
    public void getEntidade(HttpServletRequest request, HttpServletResponse response) {
        if (tipo != null && !tipo.isEmpty()) {
            if (tipo.equalsIgnoreCase("Categoria")) {
                if (chave == null || chave.isEmpty()) {
                    p = new Categoria(Codigo, Nome, Descricao);
                } else {
                    p = new Categoria(chave, Nome, Descricao);
                }
            }
        }
    }
}
