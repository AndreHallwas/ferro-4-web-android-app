/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controladora;

import UI.ValidaLogin;
import UI.painelCampos;
import UI.painelCentralList;
import UI.painelCentralView;
import UI.painelLateralOpcoes;
import UI.painelTopoOpcoes;
import entidades.Categoria;
import entidades.Cliente;
import entidades.Login;
import entidades.Pessoa;
import entidades.Produto;
import entidades.Usuario;
import entidades.Venda;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Raizen
 */
@MultipartConfig(
    location="/", 
    fileSizeThreshold=1024*1024,    // 1 MB
    maxFileSize=1024*1024*100,       // 100 MB 
    maxRequestSize=1024*1024*10*10    // 100     MB
)
@WebServlet(name = "CtrlInterface", urlPatterns = {"/CtrlInterface"})
public class CtrlInterface extends HttpServlet {

    public static painelTopoOpcoes pnTopoO = new painelTopoOpcoes();
    public static painelLateralOpcoes pnLateralO = new painelLateralOpcoes();
    public static painelCentralList pnCentralL = new painelCentralList();
    public static painelCampos pnCampos = new painelCampos();
    public static painelCentralView pnCentralV = new painelCentralView();

    public static painelTopoOpcoes getPnTopoO() {
        return pnTopoO;
    }

    public static painelLateralOpcoes getPnLateralO() {
        return pnLateralO;
    }

    public static painelCentralList getPnCentralL() {
        return pnCentralL;
    }

    public static painelCampos getPnCampos() {
        return pnCampos;
    }

    public static painelCentralView getPnCentralV() {
        return pnCentralV;
    }

    private String erro;
    public static String ServletContext = "";

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        erro = "";
        String acao = request.getParameter("acao");
        String tipo = request.getParameter("tipo");
        String param = request.getParameter("param");
        String chave = request.getParameter("chave");
        ServletContext = getServletContext().getRealPath("/");
        switch (acao) {
            case "painelTopoOpcoes":
                response.setContentType("text/html;charset=utf-8");
                response.getWriter().println(getPnTopoO().getPainelTopoOpcoes());
                return;
            case "painelLateralOpcoes":
                response.setContentType("text/html;charset=utf-8");
                response.getWriter().println(getPnLateralO().getPainelLateralOpcoes());
                return;
            case "painelCentralList":
                response.setContentType("text/html;charset=utf-8");
                getPnCentralL().setValue(tipo);
                if(tipo.equalsIgnoreCase("produto")){
                    painelCentralList.setParam(null);
                }
                /////response.getWriter().println(getPnCentralL().getPainelCentralList());
                response.sendRedirect("list.jsp");
                return;
            case "painelCampos":
                response.setContentType("text/html;charset=utf-8");
                getPnCampos().setValue(tipo);
                getPnCampos().setAcao(param);
                if (param.equalsIgnoreCase("alterar")) {
                    if (tipo.equalsIgnoreCase("usuario") || tipo.equalsIgnoreCase("cliente")) {
                        Pessoa p = tipo.equalsIgnoreCase("usuario") ? new Usuario() : new Cliente();
                        getPnCampos().setEntidade(CtrlPessoa.getEntidadeE(chave, p));
                    } else if (tipo.equalsIgnoreCase("produto")) {
                        getPnCampos().setEntidade(CtrlProduto.getEntidadeE(chave, new Produto()));
                    } else if (tipo.equalsIgnoreCase("categoria")) {
                        getPnCampos().setEntidade(CtrlCategoria.getEntidadeE(chave, new Categoria()));
                    } else if (tipo.equalsIgnoreCase("venda")) {
                        getPnCampos().setEntidade(CtrlCategoria.getEntidadeE(chave, new Venda()));
                    } 
                }
                response.sendRedirect("insert.jsp");
                return;
            case "painelCentralView":
                response.setContentType("text/html;charset=utf-8");
                getPnCentralV().setValue(tipo);
                if (param.equalsIgnoreCase("visualizar")) {
                    if (tipo.equalsIgnoreCase("usuario") || tipo.equalsIgnoreCase("cliente")) {
                        Pessoa p = tipo.equalsIgnoreCase("usuario") ? new Usuario() : new Cliente();
                        getPnCentralV().setEntidade(CtrlPessoa.getEntidadeE(chave, p));
                    } else if (tipo.equalsIgnoreCase("produto")) {
                        getPnCentralV().setEntidade(CtrlProduto.getEntidadeE(chave, new Produto()));
                    } else if (tipo.equalsIgnoreCase("categoria")) {
                        getPnCentralV().setEntidade(CtrlCategoria.getEntidadeE(chave, new Categoria()));
                    } else if (tipo.equalsIgnoreCase("venda")) {
                        getPnCentralV().setEntidade(CtrlCategoria.getEntidadeE(chave, new Venda()));
                    }
                }
                response.sendRedirect("view.jsp");
                return;
            case "venda":
                if (param != null && !param.equals("")) {
                    if (param.equalsIgnoreCase("adicionar")) {
                        ((Cliente) Login.getIndividuo()).getCarrinho().add((Produto) CtrlProduto.getEntidadeE(chave, new Produto()), 1);
                    }else if (param.equalsIgnoreCase("vender")) {
                        CtrlVenda cv = new CtrlVenda();
                        cv.vender(((Cliente) Login.getIndividuo()), 1);
                        ((Cliente) Login.getIndividuo()).setCarrinho(null);
                    }
                }
                response.sendRedirect("venda.jsp");
                return;
            case "carrinho":
                if (param != null && !param.equals("")) {
                    if (param.equalsIgnoreCase("excluir")) {
                        ((Cliente) Login.getIndividuo()).getCarrinho().remove((Produto) CtrlProduto.getEntidadeE(chave, new Produto()));
                    }
                }
                response.sendRedirect("carrinho.jsp");
                return;
        }
        response.sendRedirect("index.jsp");
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
