/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controladora;

import static Controladora.CtrlEntidade.p;
import entidades.Categoria;
import entidades.Entidade;
import entidades.Estoque;
import entidades.Produto;
import entidades.Venda;
import java.util.ArrayList;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Aluno
 */
@MultipartConfig(
    location="/", 
    fileSizeThreshold=1024*1024,    // 1 MB
    maxFileSize=1024*1024*100,       // 100 MB 
    maxRequestSize=1024*1024*10*10    // 100     MB
)
@WebServlet(name = "CtrlProduto", urlPatterns = {"/CtrlProduto"})
public class CtrlProduto extends CtrlEntidade {

    private String Codigo;
    private String Nome;
    private String Descricao;
    private Estoque Estoque;
    private Categoria Categoria;
    private String chave;
    private String tipo;
    private String Valor;

    @Override
    public void getParametros(HttpServletRequest request, HttpServletResponse response) {
        acao = request.getParameter("acao");
        id = request.getParameter("id");
        tipo = request.getParameter("tipo");
        chave = request.getParameter("chave");
        Nome = request.getParameter("Nome");
        Codigo = request.getParameter("Codigo");
        Valor = request.getParameter("Valor");
        Descricao = request.getParameter("Descricao");
        /*Estoque = new Estoque(request.getParameter("estoque"));*/
        Categoria = (Categoria) CtrlCategoria.getEntidadeE(request.getParameter("Categoria"), new Categoria());
    }

    @Override
    public void getEntidade(HttpServletRequest request, HttpServletResponse response) {
        if (tipo != null && !tipo.isEmpty()) {
            if (tipo.equalsIgnoreCase("Produto")) {
                if (chave == null || chave.isEmpty()) {
                    p = new Produto(Codigo, Nome, Descricao, Estoque, Categoria, Valor);
                } else {
                    p = new Produto(chave, Nome, Descricao, Estoque, Categoria, Valor);
                }
            }
        }
    }
    
    public ArrayList<Entidade> pesquisar(String Filtro){
        Produto produto = new Produto();
        return produto.get(Filtro);
    }
}
