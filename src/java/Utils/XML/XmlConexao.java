/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Utils.XML;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.io.Writer;
import java.net.HttpURLConnection;
import java.net.URL;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Raizen
 */
public class XmlConexao {

    public void EnviaXml() {
        try {  //URL url = new URL("http://www2.unoeste.br/~silvio/recebe.php");   //PHP
            URL url = new URL("http://localhost:8080/MostrarData/RecebeXML"); //JAVA
            HttpURLConnection httpConn = (HttpURLConnection) url.openConnection();
            httpConn.setDoOutput(true);

            String xml = "<?xml version='1.0' encoding='UTF-8'?><mensagem> bla bla bla </mensagem>";
            Writer connWriter = new OutputStreamWriter(httpConn.getOutputStream());
            connWriter.write("xml=" + xml);
            connWriter.flush();
            connWriter.close();

            int responseCode = httpConn.getResponseCode();
            if (responseCode != 200) {
                System.out.println(httpConn.getResponseMessage());
            }

            BufferedReader br = new BufferedReader(new InputStreamReader(httpConn.getInputStream()));
            String linha, resultado = "";
            while ((linha = br.readLine()) != null) {
                resultado = resultado + linha;
            }
            br.close();
            System.out.println(resultado);
        } catch (Exception e) {
            System.out.println(e.toString());
        }
    }

    public class RecebeXML extends HttpServlet {

        protected void processRequest(HttpServletRequest request,
                HttpServletResponse resp) throws ServletException, IOException {
            resp.setContentType("text/html;charset=UTF-8");
            PrintWriter out = resp.getWriter();
            try {
                String xml = request.getParameter("xml");
                out.println("Arquivo recebido");
                if (xml.startsWith("<?xml")) {
                    out.println("Arquivo válido");
                } else {
                    out.println("Arquivo inválido");
                }
            } finally {
                out.close();
            }
        }
    }

    /*<?php
   // recebe o parâmetro $xml
   print_r("Recebido");
   // faz uma validação simples...
   $pos=strpos($xml,"<?xml");
   if($pos===false)
      print_r(" arquivo inválido: ");
   else
   {  print_r(" arquivo válido: ");
      // aqui podemos colocar código para processar o XML
      //  (gravar,inserir em BD, gerar um HTML, ...)
   } 
   print_r($xml);
    ?>
     *//*
    class janelaswing extends JFrame {

        JEditorPane pagina; // browser HTML

        public janelaswing(String sURL) {
            this.setSize(800, 600);
            pagina = new JEditorPane();
            pagina.setContentType(“text / html
            ”); // necessário qdo utilizar o
     pagina.setEditable(false);          // setText() ao invés do
            try // setPage()
            {
                pagina.setPage(sURL);
            } catch (Exception ex) {
                System.out.println("Erro");
            }
            setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
            getContentPane().add(new JScrollPane(pagina), BorderLayout.CENTER);
        }
    }*/

}
