/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Utils.XML;

import WSPedidos.Pedido;
import com.thoughtworks.xstream.XStream;

/**
 *
 * @author Raizen
 */
public class XmlXStream {

    public static XStream Compacta(String Nome, Object objeto) {
        XStream xstream = new XStream();
        xstream.alias(Nome, objeto.getClass());
        return xstream;
    }

    public static String GeraXml(String Nome, Object objeto) {
        /*xstream.useAttributeFor(Monitor.class, "marca");*/
        return Compacta(Nome, objeto).toXML(objeto);
    }

    public static Object GeraObjeto(Class objeto, String xml) {
        Object obj = null;
        try {
            obj = objeto.newInstance();
            XStream xstream = new XStream();
            obj = xstream.fromXML(xml);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return obj;
    }

    public static Object GeraObjeto(String xml) {
        Object obj = null;
        XStream xstream = new XStream();
        xstream.alias("Tabela", Pedido.class);
        obj = xstream.fromXML(xml);
        return obj;
    }

}
