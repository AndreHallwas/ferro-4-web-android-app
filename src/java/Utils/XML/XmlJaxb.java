/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Utils.XML;

import java.io.File;
import java.util.ArrayList;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;

/**
 *
 * @author Raizen
 */
public class XmlJaxb {

    private static ArrayList<Object> personData = new ArrayList();

    public static void loadPersonDataFromFile(File file) {
        try {
            JAXBContext context = JAXBContext
                    .newInstance(Object.class);///PersonListWrapper
            Unmarshaller um = context.createUnmarshaller();

            // Reading XML from the file and unmarshalling.
            Object wrapper = (Object) um.unmarshal(file);///PersonListWrapper

            personData.clear();
            personData.add(wrapper);////.getPersons()

        } catch (Exception e) {
            System.out.println("Could not load data from file:\n"
                    + file.getPath());
        }
    }

    public static void savePersonDataToFile(File file) {
        try {
            JAXBContext context = JAXBContext
                    .newInstance(Object.class);
            Marshaller m = context.createMarshaller();
            m.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);

            // Wrapping our person data.
            Object wrapper = new Object();///new PersonListWrapper()
            ////wrapper.setPersons(personData);

            // Marshalling and saving XML to the file.
            m.marshal(wrapper, file);/////venda,System.out

        } catch (Exception e) {
            System.out.println("Could not save data to file:\n" + file.getPath());
        }
    }

    /*
    * public static void main(String[] args) {
        personData.add(new Person("Hans", "Muster"));
        personData.add(new Person("Ruth", "Mueller"));
        personData.add(new Person("Heinz", "Kurz"));
        personData.add(new Person("Cornelia", "Meier"));
        personData.add(new Person("Werner", "Meyer"));
        personData.add(new Person("Lydia", "Kunz"));
        personData.add(new Person("Anna", "Best"));
        personData.add(new Person("Stefan", "Meier"));
        personData.add(new Person("Martin", "Mueller"));
        savePersonDataToFile(new File("d:\\pessoas.xml"));
    }
     *//*
    @XmlJavaTypeAdapter(LocalDateAdapter.class)
    public LocalDate getBirthday() { return birthday;}
    class LocalDateAdapter extends XmlAdapter<String, LocalDate> {

        @Override
        public LocalDate unmarshal(String v) throws Exception {
            return LocalDate.parse(v);
        }

        @Override
        public String marshal(LocalDate v) throws Exception {
            return v.toString();
        }
    }

    @XmlRootElement(name = "persons")

    public class PersonListWrapper {

        private List<Person> persons;

        @XmlElement(name = "person")
        public List<Person> getPersons() {
            return persons;
        }

        public void setPersons(List<Person> persons) {
            this.persons = persons;
        }
    }*/

}
