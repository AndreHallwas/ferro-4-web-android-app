/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Utils.XML;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.io.StringReader;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.XMLConstants;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import javax.xml.validation.Validator;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.ErrorHandler;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.SAXParseException;

/**
 *
 * @author Raizen
 */
public class XmlDom {

    private static void stepThrough(Node start) {
        System.out.println(start.getNodeName() + " = " + start.getNodeValue());
        for (Node child = start.getFirstChild(); child != null;
                child = child.getNextSibling()) { //Recuperando os filhos recursivamente
            stepThrough(child);
        }
    }

    public void Leitura(String args[]) {
        DocumentBuilderFactory factory;
        DocumentBuilder builder;
        Document doc = null;
        try {
            factory = DocumentBuilderFactory.newInstance();
            builder = factory.newDocumentBuilder();
            doc = builder.parse("cds.xml");
        } catch (Exception e) {
            System.out.println(e.toString());
            System.exit(1);
        }
        Element root = doc.getDocumentElement();
        System.out.println("O elemento raiz é " + root.getNodeName());
        //Recupera os filhos (children)
        NodeList children = root.getChildNodes();
        System.out.println("Existem " + children.getLength() + " nós ");
        stepThrough(root);
    }

    public void Limpar() {
        DocumentBuilderFactory factory;
        DocumentBuilder builder;
        Document doc = null;
        try {
            factory = DocumentBuilderFactory.newInstance();
            builder = factory.newDocumentBuilder();
            RandomAccessFile arq = new RandomAccessFile("d:\\livros.xml", "r");
            byte bytes[] = new byte[(int) arq.length()];
            arq.read(bytes);
            String conteudo = new String(bytes);
            conteudo = conteudo.replaceAll(">\\s*<", "><");
            doc = builder.parse(new InputSource(new StringReader(conteudo)));
        } catch (Exception e) {
            System.out.println(e.toString());
        }
    }

    public void GravaXml() {
        DocumentBuilderFactory factory;
        DocumentBuilder builder;
        Document doc = null;
        try {
            factory = DocumentBuilderFactory.newInstance();
            builder = factory.newDocumentBuilder();
            doc = builder.newDocument();
        } catch (Exception e) {
            System.out.println(e.toString());
        }
        Element raiz = doc.createElement("publicacao");
        Element cap_elemento = doc.createElement("capitulo");
        Element aut_elemento = doc.createElement("autor");
        cap_elemento.appendChild(doc.createTextNode("Capitulo 1: Introducao"));
        aut_elemento.appendChild(doc.createTextNode("Manoel das Cruzes"));
        raiz.setAttribute("tipo", "livro");
        raiz.appendChild(cap_elemento);
        raiz.appendChild(aut_elemento);
        doc.appendChild(doc.createComment("Exemplo de criação de XML com DOM"));
        doc.appendChild(raiz);
        try {
            Transformer t = TransformerFactory.newInstance().newTransformer();
            t.setOutputProperty(OutputKeys.INDENT, "yes");
            // grava em arquivo
            t.transform(new DOMSource(doc), new StreamResult("arq.xml"));
            /*/////GravaString
            StringWriter xmlString=new StringWriter();
            t.transform(new DOMSource(doc),new StreamResult(xmlString));
            System.out.println(xmlString);
             */
        } catch (Exception ex) {
            System.out.println(ex.toString());
        }

    }

    public void Transforma() {
        try {
            TransformerFactory factory = TransformerFactory.newInstance();
            Transformer transformer = factory.newTransformer(new StreamSource("pessoas_template.xsl"));
            transformer.transform(new StreamSource("pessoas.xml"),
                    new StreamResult(new FileOutputStream("pessoas.html")));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void ValidarXmlDtd(String xml) {
        DocumentBuilderFactory factory;
        DocumentBuilder builder;
        Document doc = null;
        try {
            factory = DocumentBuilderFactory.newInstance();
            builder = factory.newDocumentBuilder();
            factory.setValidating(true);
            builder.setErrorHandler(new MyErrorHandler());
            doc = builder.parse("biblio.xml");
        } catch (IOException e) {
            System.err.println("Erro de IO:" + e);
        } catch (SAXParseException spe) {
            System.out.println("\n** Erro no parsing "
                    + ", linha " + spe.getLineNumber() + ", uri " + spe.getSystemId());
            System.out.println("   " + spe.getMessage());
        } catch (SAXException e) {
            System.err.println("Erro de xml:" + e);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void ValidarXmlXsd(String xml) {
        try {  // Parse an XML document into a DOM tree.
            DocumentBuilder parser
                    = DocumentBuilderFactory.newInstance().newDocumentBuilder();
            //Document document = parser.parse(new File(“d:\tempo.xml”));
            Document document
                    = parser.parse(new InputSource(new StringReader(xml))); // doc. xml na string
            // Create a SchemaFactory capable of understanding WXS schemas.
            SchemaFactory factory
                    = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);
            // Load a WXS schema, represented by a Schema instance.
            Source schemaFile = new StreamSource(new File("d:/tempo.xsd"));
            Schema schema = factory.newSchema(schemaFile);
            // Create a Validator object, can be used to validate an instance document.
            Validator validator = schema.newValidator();
            // Validate the DOM tree.
            validator.validate(new DOMSource(document));

        } catch (ParserConfigurationException ex) {
            Logger.getLogger(XmlDom.class.getName()).log(Level.SEVERE, null, ex);
        } catch (SAXException ex) {
            Logger.getLogger(XmlDom.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(XmlDom.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void Unir() {
        Document bigdoc = null, littledoc = null;
        // bigdoc representa o grande documento, 
        // o qual envolve várias instancias de littledoc
        DocumentBuilderFactory factory;
        DocumentBuilder builder;
        try {
            factory = DocumentBuilderFactory.newInstance();
            builder = factory.newDocumentBuilder();
            // abre os dois XMLs
            bigdoc = builder.parse("cds.xml");
            littledoc = builder.parse("cd.xml");
            Element BigRoot = bigdoc.getDocumentElement();
            Element LittleRoot = littledoc.getDocumentElement();
            // BigRoot é o raiz do documento grande, LittleRoot é o raiz do documento pequeno
            Element aux = (Element) bigdoc.importNode(LittleRoot, true);
            //aux representa uma cópia do documento pequeno; aux é adicionado ao raiz do doc. grande
            BigRoot.appendChild(aux);
            Transformer t = TransformerFactory.newInstance().newTransformer();
            t.setOutputProperty(OutputKeys.INDENT, "yes");
            // grava (atualiza) o documento grande em arquivo
            t.transform(new DOMSource(bigdoc), new StreamResult("cds.xml"));
        } catch (Exception e) {
            System.out.println("Erro: " + e.getMessage());
        }
    }

    class MyErrorHandler implements ErrorHandler {

        @Override
        public void fatalError(SAXParseException exception) throws SAXException {
            throw exception;
        }

        @Override
        public void error(SAXParseException e) throws SAXParseException {
            throw e;
        }

        @Override
        public void warning(SAXParseException err) throws SAXParseException {
            System.err.println("Warning: " + err.getMessage());
        }
    }

}
