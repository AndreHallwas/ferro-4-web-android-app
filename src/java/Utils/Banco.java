/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Utils;

/**
 *
 * @author Raizen
 */
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.RandomAccessFile;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import javafx.application.Platform;
import javafx.scene.control.TextArea;

public class Banco
{

    static private Conexao con = null;
    static public int StatsConexao;

    private Banco()
    {

    }
    
    static public Conexao getCon(){
        conectar();
        return con;
    }
    
    static public boolean conectar()
    {
        if (con == null)
        {
            con = new Conexao();
            return con.conectar("jdbc:postgresql://localhost:5432/", "ferro4",
                    "postgres", "postgres123");
        }
        return true;
    }

    public static int iniciarConexao(String nomeBD, String nomeArquivoBanco)
    {
        StatsConexao = Banco.PreConexao();
        if (StatsConexao == 0)
            if (Banco.criarBD(nomeBD))
                if (Banco.criarTabelas(nomeArquivoBanco, nomeBD))
                {
                    if (Banco.realizaBackupRestauracao("restaurar.bat"))
                        Mensagem.ExibirLog("Restore Concluído");
                    else
                        Mensagem.ExibirLog("Restore Não Concluído");
                    StatsConexao = 1;
                } else
                    Mensagem.ExibirLog("FATAL ERROR: Erro ao criar as tabelas");
            else
                Mensagem.ExibirLog(Banco.getCon().getMensagemErro());
        return StatsConexao;
    }

    public static int PreConexao()
    {
        ArrayList<String> Lista = Arquivo.leArquivoDeStringUTF("Config.txt", 6);
        if (Lista != null && !Lista.isEmpty())
            return conectar(Lista.get(5), Lista.get(0), Lista.get(1), Lista.get(3), Lista.get(4), Lista.get(2)) == true ? 1 : 0;
        return 2;
    }

    public static boolean conectar(String stringConexao, String endereco, String porta, String usuarioBanco, String senhaBanco, String banco)
    {
        if (con == null)
        {
            con = new Conexao();
            return con.conectar(stringConexao + "://" + endereco + ":" + porta + "/", banco,
                    usuarioBanco, senhaBanco);
        }
        return true;
    }

    public static boolean criarBD(String BD)
    {
        try
        {
            //Class.forName("org.postgresql.Driver");
            String url = "jdbc:postgresql://localhost/";
            Connection con = DriverManager.getConnection(url, "postgres", "postgres123");

            Statement statement = con.createStatement();
            statement.execute("CREATE DATABASE " + BD + " WITH OWNER = postgres ENCODING = 'UTF8'  "
                    + "TABLESPACE = pg_default LC_COLLATE = 'Portuguese_Brazil.1252'  "
                    + "LC_CTYPE = 'Portuguese_Brazil.1252'  CONNECTION LIMIT = -1;");
            statement.close();
            con.close();
        } catch (SQLException e)
        {
            Mensagem.ExibirException(e);
            return false;
        }
        return true;
    }

    public static boolean criarTabelas(String script, String BD)
    {
        try
        {
            //Class.forName("org.postgresql.Driver");
            String url = "jdbc:postgresql://localhost/" + BD;
            Connection con = DriverManager.getConnection(url, "postgres", "postgres123");

            Statement statement = con.createStatement();
            RandomAccessFile arq = new RandomAccessFile(script, "r");
            while (arq.getFilePointer() < arq.length())
            {
                statement.addBatch(arq.readLine());
            }
            statement.executeBatch();

            statement.close();
            con.close();
        } catch (IOException | SQLException e)
        {
            Mensagem.ExibirException(e, "Erro ao criar tabela: ");
            return false;
        }
        return true;
    }

    public static boolean realizaBackupRestauracao(String arqlote, TextArea ta)
    {
        Runtime r = Runtime.getRuntime();
        try
        {
            Process p = r.exec("bkp\\" + arqlote);
            if (p != null)
            {
                InputStreamReader str = new InputStreamReader(p.getErrorStream());
                BufferedReader reader = new BufferedReader(str);
                String linha;
                while ((linha = reader.readLine()) != null)
                {
                    System.out.println(linha);
                    final String lin = linha;
                    Platform.runLater(() ->
                    {
                        ta.appendText(lin + "\n");
                    });
                }
            }
        } catch (IOException ex)
        {
            Mensagem.ExibirException(ex, "Erro ao Realizar Backup");
            return false;
        }
        return true;
    }

    public static boolean realizaBackupRestauracao(String arqlote)
    {
        Runtime r = Runtime.getRuntime();
        try
        {
            Process p = r.exec("bkp\\" + arqlote);
        } catch (IOException ex)
        {
            Mensagem.ExibirException(ex, "Erro ao Realizar Restauração");
            return false;
        }
        return true;
    }

}