/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package WSPedidos;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamImplicit;

/**
 *
 * @author Raizen
 */
@XStreamAlias("Pedido")
public class Pedido {
    @XStreamImplicit(itemFieldName = "Nome")
    private String Nome;
    @XStreamImplicit(itemFieldName = "Valor")
    private Double Valor;
    @XStreamImplicit(itemFieldName = "Codigo")
    private String Codigo;
    @XStreamImplicit(itemFieldName = "Quantidade")
    private int Quantidade;

    public Pedido() {
        
    }

    public Pedido(String Codigo, String Nome, String Quantidade, String valor) {
        Nome = Nome;
        Valor = Double.parseDouble(valor);
        this.Quantidade = Integer.parseInt(Quantidade);
        this.Codigo = Codigo;
    }

    public Pedido(String nome, Double valor, String Codigo, int Quantidade) {
        Nome = nome;
        Valor = valor;
        this.Quantidade = Quantidade;
        this.Codigo = Codigo;
    }

    @Override
    public String toString() {
        return "Pedido{" +
                "Nome='" + Nome + '\'' +
                ";; Valor=" + Valor +
                ";; Codigo='" + Codigo + '\'' +
                ";; Quantidade=" + Quantidade +
                '}';
    }

    public String getNome() {
        return Nome;
    }

    public void setNome(String nome) {
        Nome = nome;
    }

    public String getCodigo() {
        return Codigo;
    }

    public void setCodigo(String codigo) {
        Codigo = codigo;
    }

    public Double getValor() {
        return Valor;
    }

    public void setValor(Double valor) {
        Valor = valor;
    }

    public int getQuantidade() {
        return Quantidade;
    }

    public void setQuantidade(int quantidade) {
        Quantidade = quantidade;
    }
}
