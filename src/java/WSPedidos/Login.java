/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package WSPedidos;

/**
 *
 * @author Raizen
 */
public class Login {
    private String cpf;
    private String pass;

    public Login(String CPF, String Senha) {
        this.cpf = CPF;
        this.pass = Senha;
    }

    public String getCpf() {
        return cpf;
    }

    public void setCpf(String cpf) {
        this.cpf = cpf;
    }

    public String getPass() {
        return pass;
    }

    public void setPass(String senha) {
        this.pass = senha;
    }
    
    
}