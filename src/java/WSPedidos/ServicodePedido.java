/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package WSPedidos;

import Controladora.CtrlPessoa;
import Controladora.CtrlProduto;
import Controladora.CtrlVenda;
import Utils.XML.XmlXStream;
import entidades.Carrinho;
import entidades.Cliente;
import entidades.Entidade;
import entidades.Produto;
import entidades.Usuario;
import java.util.ArrayList;
import javax.jws.WebService;
import javax.jws.WebMethod;
import javax.jws.WebParam;

/**
 *
 * @author Raizen
 */
@WebService(serviceName = "ServicodePedido")
public class ServicodePedido {

    /**
     * Operação de Web service
     */
    @WebMethod(operationName = "realizarPedido")
    public String realizarPedido(@WebParam(name = "Dados") Object Dados) {
        //TODO write your implementation code here:
        String[] Pedidos = ((String)Dados).split("╣╣");
        Cliente c = null;
        Carrinho carrinho = new Carrinho();
        Object Teste;
        Pedido pedido;
        for (String pedid : Pedidos) {
            Teste = XmlXStream.GeraObjeto(pedid);
            if(Teste instanceof Login){
                c = (Cliente) CtrlPessoa.getEntidadeE(((Login)Teste).getCpf(), new Cliente());
            }else{
                carrinho.add((Produto) 
                        CtrlProduto.getEntidadeE(((Pedido)Teste).getCodigo(), 
                                new Produto()), ((Pedido)Teste).getQuantidade());
            }
        }
        if(c != null){
            c.setCarrinho(carrinho);
            CtrlVenda cv = new CtrlVenda();
            cv.vender(c, 1);
            return "Pedido Efetuado com Sucesso";
        }
        return "Pedido Não Efetuado";
    }

    /**
     * Operação de Web service
     */
    @WebMethod(operationName = "login")
    public String login(@WebParam(name = "CPF") String CPF, @WebParam(name = "Senha") String Senha) {
        //TODO write your implementation code here:
        Cliente cliente = (Cliente) CtrlPessoa.getEntidadeE(CPF, new Cliente());
        return cliente.getSenha().equalsIgnoreCase(Senha) ? "Logado" : "CPF ou Senha Inválidos";
    }

    /**
     * Operação de Web service
     */
    @WebMethod(operationName = "getProdutos")
    public ArrayList<Pedido> getProdutos() {
        //TODO write your implementation code here:
        ArrayList<Pedido> Pedidos = new ArrayList<Pedido>();
        CtrlProduto cp = new CtrlProduto();
        ArrayList<Entidade> Produtos = cp.pesquisar("");
        for (Entidade Produto : Produtos) {
            Produto produto = (Produto) Produto;
            Pedido pedido = new Pedido(produto.getNome(),Double.parseDouble(produto.getValor()), produto.getCodigo(),1);
            Pedidos.add(pedido);
        }
        Pedidos.add(new Pedido("Nome",50.0,"1",2));
        Pedidos.add(new Pedido("Nome",50.0,"1",2));
        return Pedidos;
    }
}
