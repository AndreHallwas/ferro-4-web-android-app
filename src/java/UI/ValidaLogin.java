/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package UI;

import entidades.Cliente;
import entidades.Login;
import entidades.Pessoa;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Aluno
 */
@WebServlet(name = "ValidaLogin", urlPatterns = {"/ValidaLogin"})
public class ValidaLogin extends HttpServlet {
    public static Pessoa pessoa = null;
    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try{
            String acao = request.getParameter("acao");
            if(null == acao||acao.equals("logar")){
                String Usuario = request.getParameter("Usr");
                String Senha = request.getParameter("Senha");
                Login.logar(new Cliente(Usuario, Usuario, Usuario, Usuario, Usuario, Usuario, Senha));
                if(Usuario.length()>5 && Usuario.length()<9 && Senha.length() == 4 && verificaLetraNum(Senha)){
                    response.sendRedirect("Dados");
                }else{
                    response.sendRedirect("index.jsp");
                    return;
                }
            }else if(acao.equals("deslogar")){
                Login.deslogar();
            }
        }catch(Exception ex){ }
        response.sendRedirect("index.jsp");
        
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

    private boolean verificaLetraNum(String Senha) {
        boolean v1 = false, v2 = false;
        for (int i = 0; i < Senha.length() && !(v1&&v2); i++) {
            if(Character.isDigit(Senha.charAt(i))){
                v1 = true;
            }else if(Character.isLetter(Senha.charAt(i))){
                v2 = true;
            }
        }
        return v1&&v2;
    }

}
