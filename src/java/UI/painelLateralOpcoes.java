/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package UI;

/**
 *
 * @author Raizen
 */
public class painelLateralOpcoes {

    private static String value = "default";

    public String getPainelLateralOpcoes() {
        String resposta = "";
        if (value.equals("default")) {
            resposta += "<ul class='nav navbar-nav navbar-left'> ";
            resposta += "<h5><li class='btn' style='color:white'>" + "Gerenciamento" + "</li></h5>";
            resposta += "<h6><li><a href='" + "CtrlInterface?tipo=usuario&acao=painelCentralList" + "'>Usuario</a></li></h6>";
            resposta += "<h6><li><a href='" + "CtrlInterface?tipo=cliente&acao=painelCentralList" + "'>Cliente</a></li></h6>";
            resposta += "<h6><li><a href='" + "CtrlInterface?tipo=produto&acao=painelCentralList" + "'>Produto</a></li></h6>";
            resposta += "<h6><li><a href='" + "CtrlInterface?tipo=categoria&acao=painelCentralList" + "'>Categoria</a></li></h6>";
            resposta += "<h5><li class='btn' style='color:white'>" + "Funções" + "</li></h5>";
            resposta += "<h6><li><a href='" + "CtrlInterface?tipo=venda&acao=venda" + "'>Venda</a></li></h6>";
            resposta += "<h6><li><a href='" + "CtrlInterface?tipo=carrinho&acao=carrinho" + "'>Carrinho</a></li></h6>";
            resposta += "<h5><li class='btn' style='color:white'>" + "Relatórios" + "</li></h5>";
            resposta += "<h6><li><a href='" + "#" + "'>Geral</a></li></h6>";
            resposta += "<h6><li><a href='" + "CtrlInterface?tipo=venda&acao=painelCentralList" + "'>Vendas</a></li></h6>";
            resposta += "</ul>";
        }
        return resposta;
    }
}
