/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package UI;

import entidades.Login;

/**
 *
 * @author Raizen
 */
public class painelTopoOpcoes {
    private static String value = "default";
    
    public String getPainelTopoOpcoes() {
        String Resposta = "";
        if(value.equals("default")){
            Resposta += "<span class='sr-only'>Toggle navigation</span>";
            Resposta += "<li><a href='" + "index.jsp" + "'>Início</a></li>";
            Resposta += Login.isLogado()? "<li><a href='" + "ValidaLogin?acao=deslogar" + "'>Deslogar</a></li>" :
                    "<li><a href='" + "TelaLogin.jsp?acao=logar" + "'>Login</a></li>";
        }
        return Resposta;
    }
}
