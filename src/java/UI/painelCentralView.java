/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package UI;

import Controladora.CtrlInterface;
import Controladora.CtrlPessoa;
import Controladora.CtrlProduto;
import entidades.Categoria;
import entidades.Cliente;
import entidades.Entidade;
import entidades.Produto;
import entidades.Usuario;
import entidades.Venda;

/**
 *
 * @author Raizen
 */
public class painelCentralView {

    private static String value = "default";
    private static Entidade entidade = null;

    public static String getpainelCentralView() {
        String resposta = "";
        if (value.equals("usuario")) {
            if (entidade == null) {
                entidade = new Usuario();
            }
            resposta += ((Usuario) entidade).toHiperLineViewColumns();
            entidade = null;
        } else if (value.equals("cliente")) {
            if (entidade == null) {
                entidade = new Cliente();
            }
            resposta += ((Cliente) entidade).toHiperLineViewColumns();
            entidade = null;
        } else if (value.equals("produto")) {
            if (entidade == null) {
                entidade = new Produto();
            }
            resposta += ((Produto) entidade).toHiperLineViewColumns();
            ///CtrlInterface.ServletContext;
            String aux = "http://localhost:8084/TrabalhoFerro4/img/"+((Produto) entidade).getNome()+"%201.jpg";
            resposta += "<img style='width:30%' alt='"+aux+"' src ='"+aux+"'>";
            aux = "http://localhost:8084/TrabalhoFerro4/img/"+((Produto) entidade).getNome()+"%202.jpg";
            resposta += "<img style='width:30%' alt='"+aux+"' src ='"+aux+"'>";
            aux = "http://localhost:8084/TrabalhoFerro4/img/"+((Produto) entidade).getNome()+"%203.jpg";
            resposta += "<img style='width:30%' alt='"+aux+"' src ='"+aux+"'>";
            entidade = null;
        } else if (value.equals("categoria")) {
            if (entidade == null) {
                entidade = new Categoria();
            }
            resposta += ((Categoria) entidade).toHiperLineViewColumns();
            entidade = null;
        } else if (value.equals("venda")) {
            if (entidade == null) {
                entidade = new Venda();
            }
            resposta += ((Venda) entidade).toHiperLineViewColumns();
            entidade = null;
        }
        return resposta;
    }

    public String getValue() {
        return value;
    }

    public Entidade getEntidade() {
        return entidade;
    }

    public void setValue(String value) {
        painelCentralView.value = value;
    }

    public void setEntidade(Entidade entidade) {
        painelCentralView.entidade = entidade;
    }

}
