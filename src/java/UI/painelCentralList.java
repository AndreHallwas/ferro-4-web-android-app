/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package UI;

import Controladora.CtrlPessoa;
import entidades.Categoria;
import entidades.Cliente;
import entidades.Produto;
import entidades.Usuario;
import entidades.Venda;

/**
 *
 * @author Raizen
 */
public class painelCentralList {
    private static String value = "default";
    private static String param = "0";

    public String getPainelCentralList(){
        String resposta = "";
        if(value.equals("usuario")){
            resposta += CtrlPessoa.getEntidade("", new Usuario());
        }else
        if(value.equals("cliente")){
            resposta += CtrlPessoa.getEntidade("", new Cliente());
        }else
        if(value.equals("produto")){
            if(param == null){
                resposta += CtrlPessoa.getEntidade("", new Produto());
            }else{
                resposta += CtrlPessoa.getEntidade("", new Produto(), param);
            }
        }else
        if(value.equals("categoria")){
            resposta += CtrlPessoa.getEntidade("", new Categoria());
        }else
        if(value.equals("venda")){
            resposta += CtrlPessoa.getEntidade("", new Venda());
        }
        return resposta;
    }

    public String getPainelCentralController(){
        String resposta = "";
        if(value.equals("usuario") || value.equals("cliente")){
            resposta += "CtrlPessoa";
        }else
        if(value.equals("produto")){
            resposta += "CtrlProduto";
        }else
        if(value.equals("categoria")){
            resposta += "CtrlCategoria";
        }else
        if(value.equals("venda")){
            resposta += "CtrlVenda";
        }
        return resposta;
    }

    public static void setValue(String value) {
        painelCentralList.value = value;
    }

    public static String getValue() {
        return value;
    }

    public static String getParam() {
        return param;
    }

    public static void setParam(String param) {
        painelCentralList.param = param;
    }




}
