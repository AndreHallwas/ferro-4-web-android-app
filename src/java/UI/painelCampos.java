/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package UI;

import entidades.Categoria;
import entidades.Cliente;
import entidades.Entidade;
import entidades.Produto;
import entidades.Usuario;
import entidades.Venda;

/**
 *
 * @author Raizen
 */
public class painelCampos {
    private static String value = "default";
    private static String acao = "default";
    private static Entidade entidade = null;
    
    public String getPainelCampos(){
        String resposta = "";
        if(value.equals("usuario"))
        {
            if(entidade == null){
                entidade = new Usuario();
                resposta += ((Usuario)entidade).toHiperLineColumns(1);
            }else{
                resposta += ((Usuario)entidade).toHiperLineColumns(0);
            }
            entidade = null;
        }else
        if(value.equals("cliente"))
        {
            if(entidade == null){
                entidade = new Cliente();
                resposta += ((Cliente)entidade).toHiperLineColumns(1);
            }else{
                resposta += ((Cliente)entidade).toHiperLineColumns(0);
            }
            entidade = null;
        }else
        if(value.equals("produto"))
        {
            if(entidade == null){
                entidade = new Produto();
                resposta += ((Produto)entidade).toHiperLineColumns(1,"Img");
            }else{
                resposta += ((Produto)entidade).toHiperLineColumns(0,"Img");
            }
            entidade = null;
        }else
        if(value.equals("categoria"))
        {
            if(entidade == null){
                entidade = new Categoria();
                resposta += ((Categoria)entidade).toHiperLineColumns(1);
            }else{
                resposta += ((Categoria)entidade).toHiperLineColumns(0);
            }
            entidade = null;
        }else
        if(value.equals("venda"))
        {
            if(entidade == null){
                entidade = new Venda();
                resposta += ((Venda)entidade).toHiperLineColumns(1);
            }else{
                resposta += ((Venda)entidade).toHiperLineColumns(0);
            }
            entidade = null;
        }
        return resposta;
    }

    public void setValue(String value) {
        painelCampos.value = value;
    }

    public String getValue() {
        return value;
    }

    public String getAcao() {
        return acao;
    }

    public void setAcao(String acao) {
        painelCampos.acao = acao;
    }

    public Entidade getEntidade() {
        return entidade;
    }

    public void setEntidade(Entidade entidade) {
        painelCampos.entidade = entidade;
    }
    
    
    
    
}
